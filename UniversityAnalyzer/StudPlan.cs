﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс учебный план
    /// </summary>
    public class StudPlan
    {
        /// <summary>
        /// Список детальных описаний предметов
        /// </summary>
        private List<SubjectDetails> subjects;
        /// <summary>
        /// идентификационный номер учебного плана
        /// </summary>
        private int id;

        /// <summary>
        /// Конструктор без параметра, то же самое что запустить конструткор с аргументом 0
        /// </summary>
        public StudPlan()
            : this(0)
        {

        }

        /// <summary>
        /// Конструткор
        /// </summary>
        /// <param name="id">идентификационный номер учебного плана</param>
        public StudPlan(int id)
        {
            //инициализация
            this.id = id;
            subjects = new List<SubjectDetails>();
        }

        /// <summary>
        /// Метод добавление детального описания предмета в список
        /// </summary>
        /// <param name="sem">Номер семестра</param>
        /// <param name="man">Обязательный или нет предмет</param>
        /// <param name="eid">идентификационный номер типа зачетности</param>
        /// <param name="sub">Предмет</param>
        public void AddSubject(int sem, bool man, int eid, Subject sub)
        {
            subjects.Add(new SubjectDetails(sem, man, eid, sub));
        }
        /// <summary>
        /// Метод добавление детального описания предмета в список
        /// </summary>
        /// <param name="sem">Номер семестра</param>
        /// <param name="man">Обязательный или нет предмет</param>
        /// <param name="evalmeth">Тип отчетности</param>
        /// <param name="subj">Предмет</param>
        public void AddSubject(int sem, bool man, string evalmeth, string subj)
        {
            subjects.Add(new SubjectDetails(sem, man, evalmeth, subj));
        }

        /// <summary>
        /// Свойство для получения идентификационного номера учебного плана
        /// </summary>
        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// Свойство для получения предметов, входящих в студ. план
        /// </summary>
        public List<SubjectDetails> Subjects
        {
            get { return subjects; }
        }

        /// <summary>
        /// Своство для получения строки с информацией о предметах, содержащихся в данном плане
        /// </summary>
        public String Info
        {
            get
            {
                //складываем строки с информацией о деталях каждого предмета через знак новой строки
                StringBuilder sb = new StringBuilder();
                sb.Append(System.Environment.NewLine);
                foreach (SubjectDetails sd in subjects)
                {
                    sb.Append(sd.Info);
                    sb.Append(System.Environment.NewLine);
                }
                return sb.ToString();
            }
        }

        /// <summary>
        /// метод выдающий строку с информаией о плане для записи в файл
        /// </summary>
        /// <param name="uid">Идентификационный номер университета данного учебного плана</param>
        /// <returns>строка для записи в файл</returns>
        public String ToFile(int uid)
        {
            //суммируем все строки для записи в файл каждого детального описания предмета
            StringBuilder sb = new StringBuilder();
            foreach (SubjectDetails sd in subjects)
            {
                sb.Append(sd.ToFile(uid, id));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Число предметов
        /// </summary>
        public int SubjectCount
        {
            get { return (subjects==null)?0:subjects.Count;}
        }
    }
}
