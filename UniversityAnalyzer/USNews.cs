﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Рейтинг USNews
    /// наледуется от абстрактного класса Rating
    /// </summary>
    public class USNews:Rating
    {
        /// <summary>
        /// "прячем" список критериев
        /// </summary>
        new private List<int> criteria;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="a">Академическая репутация</param>
        /// <param name="j">Репутация у работодателей</param>
        /// <param name="l">Соотношение преподавателей и студентов</param>
        /// <param name="fl">Доля иностранных преподавателей</param>
        /// <param name="s">Доля иностранных студентов</param>
        /// <param name="c">Цитируемость в расчете на одного преподавателя</param>
        public USNews(int a, int j, int l, int fl, int s, int c)
        {
            //инициализируем данные
            criteria = new List<int>();
            criteria.Add(a);
            criteria.Add(j);
            criteria.Add(l);
            criteria.Add(fl);
            criteria.Add(s);
            criteria.Add(c);
        }

        /// <summary>
        /// метод подсчета общего рейтинга
        /// </summary>
        /// <returns>Рейтинг в виде действительного числа</returns>
        public override double Rate()
        {
            //банально считаем среднее
            int sum = 0;
            foreach (int i in criteria)
            {
                sum += i;
            }
            return sum / 6.0;
        }

        /// <summary>
        /// Свойство для получения значения Академическая репутация
        /// </summary>
        public int Academics
        {
            get { return criteria[0]; }
        }

        /// <summary>
        /// Свойство для получения значения Репутация у работодателей
        /// </summary>
        public int Job
        {
            get { return criteria[1]; }
        }

        /// <summary>
        /// Свойство для получения значения Соотношение преподавателей и студентов
        /// </summary>
        public int Lecturers
        {
            get { return criteria[2]; }
        }

        /// <summary>
        /// Свойство для получения значения Доля иностранных преподавателей
        /// </summary>
        public int ForeignLecturers
        {
            get { return criteria[3]; }
        }

        /// <summary>
        /// Свойство для получения значения Доля иностранных студентов
        /// </summary>
        public int Students
        {
            get { return criteria[4]; }
        }

        /// <summary>
        /// Свойство для получения значения Цитируемость в расчете на одного преподавателя
        /// </summary>
        public int Citations
        {
            get { return criteria[5]; }
        }

        /// <summary>
        /// Свойство для создания строки для записи в файл
        /// </summary>
        public String ToFile
        {
            get
            {
                //просто записываем значение критериев через ;
                StringBuilder sb = new StringBuilder();
                foreach (int i in criteria)
                    sb.Append(i.ToString() + ";");
                sb.Append(Rate().ToString());
                sb.Append(System.Environment.NewLine);
                return sb.ToString();
            }
        }
    }
}
