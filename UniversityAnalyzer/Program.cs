﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Drawing;


/// <summary>
/// Пространство имен программы "Анализатор университетов"
/// </summary>
namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс программа - консольное приложение
    /// </summary>
    class Program
    {
        /*
        //путь к папке с файлами
        const String DB_FOLDER = "\\db\\";
        public static void Main()
        {
            //берем адрес текущей директории с файлами
            String path = Directory.GetCurrentDirectory()+DB_FOLDER;
            //Console.WriteLine(path+DB_FOLDER);
            //создаем Базу данных по полученному пути
            DataBase db = new DataBase(path);
            //выводим информацию о первом университете (для отладки)
            Console.WriteLine(db.Universities[0].Info);
            //сохраняем всеобъекты в файлы
            db.Save();
            Console.ReadKey();
        }
        */
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new mForm());
        }
    }
}
