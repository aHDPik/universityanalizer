﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс Базы данных
    /// Считывает информацию из файлов и переводит её в Сеть объектов
    /// Записывает сеть объектов назад в файлы
    /// </summary>
    public class DataBase
    {
        /// <summary>
        /// Название файла, хранящего информацию об университете
        /// </summary>
        const String UNIV_FILE = "Universities.csv";
        /// <summary>
        /// Название файла, хранящего информацию о специальностях
        /// </summary>
        const String PROF_FILE = "Professions.csv";
        /// <summary>
        /// Название файла, хранящего информацию о рейтингах ARWU
        /// </summary>
        const String ARWU_FILE = "ARWU.csv";
        /// <summary>
        /// Название файла, хранящего информацию о рейтингах THE
        /// </summary>
        const String THE_FILE = "THE.csv";
        /// <summary>
        /// Название файла, хранящего информацию о рейтингах USNews
        /// </summary>
        const String UN_FILE = "USNews.csv";
        /// <summary>
        /// Название файла, хранящего информацию об учебных планах
        /// </summary>
        const String PLAN_FILE = "StudPlan.csv";
        /// <summary>
        /// Название файла, хранящего информацию о предметах
        /// </summary>
        const String SUB_FILE = "Subjects.csv";
        /// <summary>
        /// Название файла, хранящего информацию о виде зачетности
        /// </summary>
        const String EVAL_FILE = "EvalMethods.csv";
        /// <summary>
        /// Название файла, хранящего информацию о типах занятий
        /// </summary>
        const String LESS_FILE = "Lessontypes.csv";
        /// <summary>
        /// Заголовок файла SUB_FILE
        /// </summary>
        const String SUB_HEADER = "#SubjectID;Name;Hours;lessontypeID";
        /// <summary>
        /// Заголовок файла UNIV_FILE
        /// </summary>
        const String UNIV_HEADER = "#UniversityID;UniversityName;Address;ProfessionID";
        /// <summary>
        /// Заголовок файла PROF_FILE
        /// </summary>
        const String PROF_HEADER = "#ProfessionID;Название специальности";
        /// <summary>
        /// Заголовок файла ARWU_FILE
        /// </summary>
        const String ARWU_HEADER = "#UniversityID;Q_o_Education;Q_o_Faculty;Research_Output;Per_Capita_Performance;Total";
        /// <summary>
        /// Заголовок файла THE_FILE
        /// </summary>
        const String THE_HEADER = "#UniversityID;инновации;преподавание;исследования;цитирование;сотрудники и студенты;Total";
        /// <summary>
        /// Заголовок файла UN_FILE
        /// </summary>
        const String UN_HEADER = "#UniversityID;Академ_реп;Раб_реп;Соотн_препод;Иностр препод;Иностр студ;Цитир на препод;Total";
        /// <summary>
        /// Заголовок файла PLAN_FILE
        /// </summary>
        const String PLAN_HEADER = "#PlanID;UnivesityID;Semester;SubjectID;Evl_MethodID;mandatory";
        /// <summary>
        /// Заголовок файла EVAL_FILE
        /// </summary>
        const String EVAL_HEADER = "#Evl_MethodID;Название вида отчетности";
        /// <summary>
        /// Заголовок файла LESS_FILE
        /// </summary>
        const String LESS_HEADER = "#lessontypeID;Название типа занятий";
        const String UNIVERSITY_INFO_HEADER = "Университет;ARWU;THE;USNews;Семестр;Название предмета;Число часов;Типы занятий;Форма отчетности";
        const String PROFESSION_INFO_HEADER = "Спец;Университет;ARWU;THE;USNews;Семестр;Название предмета;Число часов;Типы занятий;Форма отчетности";
        const String SUBJECTS_INFO_HEADER="Предмет;Университет;ARWU;THE;USNews;Семестр;Форма отчетности";
        /// <summary>
        /// Список загруженных университетов
        /// </summary>
        public List<University> universities;
        /// <summary>
        /// Список загруженных предметов
        /// </summary>
        private List<Subject> subjects;
        /// <summary>
        /// Путь к директории с файлами
        /// </summary>
        public/*private*/ String folderPath;            // Обязательно заменить!!!
        /// <summary>
        /// Виды зачетности, представленные в виде хеш-таблицы
        /// где ключ - идентификационный номер
        /// значение - название вида
        /// </summary>
        private Hashtable evalMethods;
        /// <summary>
        /// Типы занятий, представленные в виде хеш-таблицы
        /// где ключ - идентификационный номер
        /// значение - название типа
        /// </summary>
        private Hashtable lessonTypes;
        /// <summary>
        /// Професии, представленные в виде хеш-таблицы
        /// где ключ - идентификационный номер
        /// значение - название професии
        /// </summary>
        private Hashtable professions;

        /// <summary>
        /// Метод загрузки университетов из файла 
        /// </summary>
        private void LoadUniversities()
        {
            //создаем список
            universities = new List<University>();
            //считываем строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + UNIV_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String s in parseStrings)
            {
                //если заголовок то не парсим
                if (first)
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы 
                    String[] univstring = s.Split(';');
                    //первый элемент ид университета
                    int id = Convert.ToInt32(univstring[0]);
                    //второй его название
                    String name = univstring[1];
                    //третий адрес
                    String address = univstring[2];
                    //четвертый ид професии
                    int pid = Convert.ToInt32(univstring[3]);
                    //создаем объект и добавляем в список
                    universities.Add(new University(name, address, id, pid));
                }
            }
        }

        /// <summary>
        /// Метод загрузки предметов из файла
        /// </summary>
        private void LoadSubjects()
        {
            //создаем новый список
            subjects = new List<Subject>();
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + SUB_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String s in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (s.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] subString = s.Split(';');
                    //первый элемент ид предмета
                    int id = Convert.ToInt32(subString[0]);
                    //второй его наименование
                    String name = subString[1];
                    //третий количество часов, отведенных на предмет
                    int hours = Convert.ToInt32(subString[2]);
                    //четвертый - идентификационный номер типа занятий
                    int lid = Convert.ToInt32(subString[3]);
                    //создаем новый объект и добавляем в список
                    subjects.Add(new Subject(name, hours, id, lid));
                }
            }
        }

        /// <summary>
        /// Загрузка рейтингов ARWU из файла
        /// </summary>
        private void LoadARWU()
        {
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + ARWU_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String s in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (s.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] arwuString = s.Split(';');
                    //первый - ид книверситета
                    int uid = Convert.ToInt32(arwuString[0]);
                    //второй уровень обраования
                    int e = Convert.ToInt32(arwuString[1]);
                    //третий - уровень факультета
                    int f = Convert.ToInt32(arwuString[2]);
                    //четвертый - уровень исследований
                    int r = Convert.ToInt32(arwuString[3]);
                    //пятый - уровень производительности 
                    int p = Convert.ToInt32(arwuString[4]);
                    int i = 0;
                    //обходим список университетов пока не найдем с нужным нам ид
                    while ((i < universities.Count) && (universities[i].ID != uid))
                        i++;
                    //если нашли то добавляем рейтинг
                    if (i < universities.Count)
                        universities[i].AddARWU(e, f, r, p);
                }
            }
        }
        /// <summary>
        /// Загрузка рейтингов THE из файла
        /// </summary>
        private void LoadTHE()
        {
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + THE_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String str in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (str.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] theString = str.Split(';');
                    //первый - ид книверситета
                    int uid = Convert.ToInt32(theString[0]);
                    // второй - инновации
                    int i = Convert.ToInt32(theString[1]);
                    //третий - преподавание
                    int l = Convert.ToInt32(theString[2]);
                    //четвертый - исследования
                    int r = Convert.ToInt32(theString[3]);
                    //пятый - цитирование
                    int c = Convert.ToInt32(theString[4]);
                    //шестой - сотрудники и студенты
                    int s = Convert.ToInt32(theString[5]);
                    //обходим список университетов пока не найдем с нужным нам ид
                    int cnt = 0;
                    while ((cnt < universities.Count) && (universities[cnt].ID != uid))
                        cnt++;
                    //если нашли то добавляем рейтинг
                    if (cnt < universities.Count)
                        universities[cnt].AddTHE(i, l, r, c, s);
                }
            }
        }

        /// <summary>
        /// Загрузка рейтингов USNews из файла
        /// </summary>
        private void LoadUSNews()
        {
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + UN_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String str in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (str.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] theString = str.Split(';');
                    //первый - ид книверситета
                    int uid = Convert.ToInt32(theString[0]);
                    //второй - Академическая репутация
                    int a = Convert.ToInt32(theString[1]);
                    //третий - Репутация у работодателей
                    int j = Convert.ToInt32(theString[2]);
                    //четвертый - Соотношение преподавателей и студентов
                    int l = Convert.ToInt32(theString[3]);
                    //пятый - Доля иностранных преподавателей
                    int fl = Convert.ToInt32(theString[4]);
                    //шестой - Доля иностранных студентов
                    int s = Convert.ToInt32(theString[5]);
                    //седьмой - Цитируемость в расчете на одного преподавателя
                    int c = Convert.ToInt32(theString[6]);
                    //обходим список университетов пока не найдем с нужным нам ид
                    int cnt = 0;
                    while ((cnt < universities.Count) && (universities[cnt].ID != uid))
                        cnt++;
                    //если нашли то добавляем рейтинг
                    if (cnt < universities.Count)
                        universities[cnt].AddUSNews(a, j, l, fl, s, c);
                }
            }
        }

        /// <summary>
        /// Загрузка учебных планов из файла
        /// </summary>
        public void LoadPlans()
        {
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + PLAN_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String str in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (str.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] planString = str.Split(';');
                    //первый - ид университета
                    int uid = Convert.ToInt32(planString[1]);
                    //второй - ид учбеонго плана
                    int pid = Convert.ToInt32(planString[0]);
                    //третий - номер семестра
                    int sem = Convert.ToInt32(planString[2]);
                    //четвертый - ид предмета
                    int sid = Convert.ToInt32(planString[3]);
                    //пятый - ид вида зачетности
                    int eval = Convert.ToInt32(planString[4]);
                    //шестой - обязательный или нет
                    bool man = Convert.ToBoolean(planString[5]);
                    int cnt = 0;
                    //обходим список университетов пока не найдем с нужным нам ид
                    while ((cnt < universities.Count) && (universities[cnt].ID != uid))
                        cnt++;
                    //если нашли и плана еще нет, то добавляем учебный план
                    if ((cnt < universities.Count) && (universities[cnt].Plan == null))
                        universities[cnt].AddPlan(pid);
                    //запоминаем план нужного университета
                    StudPlan plan = universities[cnt].Plan;
                    //обходим список предметов пока не найдем с нужным нам ид
                    cnt = 0;
                    while ((cnt < subjects.Count) && (subjects[cnt].ID != sid))
                        cnt++;
                    //добавляем нужный предмет в учебный план
                    plan.AddSubject(sem, man, eval, subjects[cnt]);
                }
            }
        }

        /// <summary>
        /// Загрузка типов зачетности из файла
        /// </summary>
        private void LoadEvals()
        {
            //создаем новую хеш-таблицу
            evalMethods = new Hashtable();
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + EVAL_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String s in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (s.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] evString = s.Split(';');
                    //первый - ид типа
                    int id = Convert.ToInt32(evString[0]);
                    //второй - наименование типа
                    String name = evString[1];
                    //добавляем значение в хеш-таблицу
                    evalMethods.Add(evString[0], evString[1]);
                }
            }
        }

        /// <summary>
        /// Загрузка видов занятий из файла
        /// </summary>
        private void LoadLessons()
        {
            //создаем новую хеш-таблицу
            lessonTypes = new Hashtable();
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + LESS_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String s in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (s.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] evString = s.Split(';');
                    //первый - ид вида
                    int id = Convert.ToInt32(evString[0]);
                    //второй - наименование вида
                    String name = evString[1];
                    //добавляем значение в хеш-таблицу
                    lessonTypes.Add(evString[0], evString[1]);
                }
            }
        }

        /// <summary>
        /// Загрузка специальностей из файла
        /// </summary>
        private void LoadProfessions()
        {
            //создаем новую хеш-таблицу
            professions = new Hashtable();
            //Емеля добавил первую специльность с id = 0 и пустым значением - забраковал
            //professions.Add(0, "");
            //считываем все строки из файла
            String[] parseStrings = File.ReadAllLines(folderPath + PROF_FILE, Encoding.GetEncoding("windows-1251"));
            //фиксируем, что читаем заголовок
            bool first = true;
            //обход построчно
            foreach (String s in parseStrings)
            {
                //если заголовок или пустая строка, то не парсим
                if ((first) || (s.Equals("")))
                    first = false;
                else
                {
                    //иначе парсим
                    //разбиваем строку на элементы
                    String[] evString = s.Split(';');
                    //первый - ид специальности
                    int id = Convert.ToInt32(evString[0]);
                    //второй - наименование специальности
                    String name = evString[1];
                    //добавляем значение в хеш-таблицу
                    professions.Add(evString[0], evString[1]);
                }
            }
        }

        /// <summary>
        /// Конструктор Базы данных
        /// </summary>
        /// <param name="folderpath">Директория с файлами для чтения</param>
        public DataBase(String folderpath)
        {
            //инициализируем
            this.folderPath = folderpath;
            //загружаем все объекты из всех файлов
            LoadLessons();
            LoadEvals();
            LoadProfessions();
            LoadUniversities();
            LoadSubjects();
            LoadARWU();
            LoadTHE();
            LoadUSNews();
            LoadPlans();
            //передаем список в статическое поле SubjectDetails для использования объектами данного класса
            SubjectDetails.allSubjects = subjects;
            //передаем список в статическое поле Subject для использования объектами данного класса
            Subject.univers = universities;
            //передаем таблицу в статическое поле University для использования объектами данного класса
            University.professions = professions;
            //передаем таблицу в статическое поле Subject для использования объектами данного класса
            Subject.lessons = lessonTypes;
            //передаем таблицу в статическое поле SubjectDetails для использования объектами данного класса
            SubjectDetails.evals = evalMethods;
        }
        /// <summary>
        /// Конструктор Базы Данных
        /// </summary>
        /// <param name="defProf">Специальность по умолчанию</param>
        /// <param name="defEval">Вид отчетности по умолчанию</param>
        /// <param name="defLes">Тип занятий по умолчанию</param>
        /// <param name="fp">Путь, по которому создать базу данных</param>
        public DataBase(string defProf, string defEval, string defLes, string fp)
        {
            folderPath = fp;
            universities = new List<University>();
            subjects = new List<Subject>();
            professions = new Hashtable();
            professions.Add("1", defProf);
            evalMethods = new Hashtable();
            evalMethods.Add("1", defEval);
            lessonTypes = new Hashtable();
            lessonTypes.Add("1", defLes);
            //передаем список в статическое поле SubjectDetails для использования объектами данного класса
            SubjectDetails.allSubjects = subjects;
            //передаем список в статическое поле Subject для использования объектами данного класса
            Subject.univers = universities;
            //передаем таблицу в статическое поле University для использования объектами данного класса
            University.professions = professions;
            //передаем таблицу в статическое поле Subject для использования объектами данного класса
            Subject.lessons = lessonTypes;
            //передаем таблицу в статическое поле SubjectDetails для использования объектами данного класса
            SubjectDetails.evals = evalMethods;
        }

        /// <summary>
        /// Свойство для получения массива Университетов
        /// </summary>
        public University[] Universities
        {
            get { return universities.ToArray(); }
        }

        /// <summary>
        /// Сохранение предметов в файл
        /// </summary>
        private void SaveSubjects(String fp)
        {
            //сначала пишем в файл заголовок
            File.WriteAllText(fp + SUB_FILE, SUB_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            //обходи каждый предмет
            foreach (Subject sb in subjects)
            {
                //записываем информацию о предмете в файл
                File.AppendAllText(fp + SUB_FILE, sb.ToFile, Encoding.GetEncoding("windows-1251"));
            }
        }

        /// <summary>
        /// Сохранение видов занятий в файл
        /// </summary>
        private void SaveLessons(String fp)
        {
            //пишем в файл заголовок
            File.WriteAllText(fp + LESS_FILE, LESS_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            //обходим все ключи хеш - таблицы
            foreach (String ie in lessonTypes.Keys)
            {
                //записываем в формате ключ;значение
                File.AppendAllText(fp + LESS_FILE, ie + ";" + lessonTypes[ie] + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            }
        }

        /// <summary>
        /// Сохранение специальностей в файл
        /// </summary>
        private void SaveProfessions(String fp)
        {
            //пишем в файл заголовок
            File.WriteAllText(fp + PROF_FILE, PROF_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            //обходим все ключи хеш - таблицы
            foreach (String ie in professions.Keys)
            {
                //записываем в формате ключ;значение
                File.AppendAllText(fp + PROF_FILE, ie + ";" + professions[ie] + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            }
        }

        /// <summary>
        /// Сохранение типов зачетностив файл
        /// </summary>
        private void SaveEvals(String fp)
        {
            //пишем в файл заголовок
            File.WriteAllText(fp + EVAL_FILE, EVAL_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            //обходим все ключи хеш - таблицы
            foreach (String ie in evalMethods.Keys)
            {
                //записываем в формате ключ;значение
                string str = ie + ";" + evalMethods[ie] + System.Environment.NewLine;
                File.AppendAllText(fp + EVAL_FILE, str, Encoding.GetEncoding("windows-1251"));
            }
        }

        /// <summary>
        /// Сохранение университетов и всего их содержимого в файл
        /// </summary>
        private void SaveUniversities(String fp)
        {
            //прописываем все заголовки необходимых файлов
            File.WriteAllText(fp + UNIV_FILE, UNIV_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            File.WriteAllText(fp + ARWU_FILE, ARWU_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            File.WriteAllText(fp + THE_FILE, THE_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            File.WriteAllText(fp + UN_FILE, UN_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            File.WriteAllText(fp + PLAN_FILE, PLAN_HEADER + System.Environment.NewLine, Encoding.GetEncoding("windows-1251"));
            //обходим все университеты
            foreach (University u in universities)
            {
                //информацию об университетах записываем в файл
                File.AppendAllText(fp + UNIV_FILE, u.ToFile, Encoding.GetEncoding("windows-1251"));
                //информацию о ARWU рейтинге университетов записываем в файл
                File.AppendAllText(fp + ARWU_FILE, u.ARWUToFile, Encoding.GetEncoding("windows-1251"));
                //информацию о THE рейтинге университетов записываем в файл
                File.AppendAllText(fp + THE_FILE, u.THEToFile, Encoding.GetEncoding("windows-1251"));
                //информацию о USNews рейтинге университетов записываем в файл
                File.AppendAllText(fp + UN_FILE, u.USNewsToFile, Encoding.GetEncoding("windows-1251"));
                //информацию об учебных планах университетов записываем в файл
                File.AppendAllText(fp + PLAN_FILE, u.PlanToFile, Encoding.GetEncoding("windows-1251"));
            }
        }

        /// <summary>
        /// Сохранить бд в указанную директорию
        /// </summary>
        /// <param name="fp">Путь к указанной директории</param>
        public void SaveToFolder(String fp)
        {
            //временно чтобы не стирать изначальные таблицы
            //folderPath = folderPath + "temp\\";
            Directory.CreateDirectory(fp);
            //сохраняем новые
            SaveEvals(fp);
            SaveLessons(fp);
            SaveProfessions(fp);
            SaveSubjects(fp);
            SaveUniversities(fp);
        }

        //сохраняем все объекты в соотвествующие файлы
        /// <summary>
        /// Сохранение в текущую директорию
        /// </summary>
        public void Save()
        {
            String fp = folderPath;
            //удаляем текущие
//             File.Delete(fp + ARWU_FILE);
//             File.Delete(fp + UNIV_FILE);
//             File.Delete(fp + UN_FILE);
//             File.Delete(fp + THE_FILE);
//             File.Delete(fp + PLAN_FILE);
//             File.Delete(fp + PROF_FILE);
//             File.Delete(fp + LESS_FILE);
//             File.Delete(fp + SUB_FILE);
//             File.Delete(fp + EVAL_FILE);
            SaveToFolder(fp); 
        }




        //Емеля добавляет методы:
        /// <summary>
        /// Возвращение таблицы с рейтингом ARWU. 
        /// </summary>
        /// <returns>Массив массивов строк где 1 элемент сопоставляется 1 клетке таблицы</returns>
        public string[][] ViewUniversityARWU()
        {
            int i = 1;
            string[][] res = new string[universities.Count + 1][];
            res[0] = new string[] { "Имя", "адрес", "Рейтинг ARWU", "Специальность" };
            foreach (University un in universities)
            {
                res[i] = new string[4];
                res[i][0] = un.Name;
                res[i][1] = un.Address;
                res[i][2] = (un.ARWU != null) ? Math.Round(un.ARWU.Rate(),2).ToString():"0";
                res[i][3] = (un.ProfId != 0) ?University.professions[un.ProfId.ToString()].ToString() : "Programmer";
                i++;
            }
            return res;
        }
        /// <summary>
        /// Возвращение таблицы с рейтингом THE. 
        /// </summary>
        /// <returns>Массив массивов строк где 1 элемент сопоставляется 1 клетке таблицы</returns>
        public string[][] ViewUniversityTHE()
        {
            int i = 1;
            string[][] res = new string[universities.Count + 1][];
            res[0] = new string[] { "Имя", "адрес", "Рейтинг THE", "Специальность" };
            foreach (University un in universities)
            {
                res[i] = new string[4];
                res[i][0] = un.Name;
                res[i][1] = un.Address;
                res[i][2] = (un.THE != null) ? Math.Round(un.THE.Rate(),2).ToString():"0";
                res[i][3] = (un.ProfId != 0) ? University.professions[un.ProfId.ToString()].ToString(): "Programmer";
                i++;
            }
            return res;
        }
        /// <summary>
        /// Возвращение таблицы с рейтингом USNews. 
        /// </summary>
        /// <returns>Массив массивов строк где 1 элемент сопоставляется 1 клетке таблицы</returns>
        public string[][] ViewUniversityUSNews()
        {
            int i = 1;
            string[][] res = new string[universities.Count + 1][];
            res[0] = new string[] { "Имя", "адрес", "Рейтинг USNews", "Специальность" };
            foreach (University un in universities)
            {
                res[i] = new string[4];
                res[i][0] = un.Name;
                res[i][1] = un.Address;
                res[i][2] = (un.USNews != null) ? Math.Round(un.USNews.Rate(),2).ToString() : "0";
                res[i][3] = (un.ProfId != 0) ? University.professions[un.ProfId.ToString()].ToString() : "Programmer";
                i++;
            }
            return res;
        }
        /// <summary>
        /// Возвращение таблицы с общим рейтингом.
        /// </summary>
        /// <returns>Массив массивов строк где 1 элемент сопоставляется 1 клетке таблицы</returns>
        public string[][] ViewUniversityCommonRating()
        {
            int i = 1;
            string[][] res = new string[universities.Count + 1][];
            res[0] = new string[] { "Имя", "адрес", "Общий рейтинг", "Специальность" };
            foreach (University un in universities)
            {
                res[i] = new string[4];
                double rate = (((un.ARWU != null) ? un.ARWU.Rate() : 0) +
                               ((un.THE != null) ? un.THE.Rate() : 0) + 
                               ((un.USNews != null) ? un.USNews.Rate() : 0)) / 3.0;
                res[i][0] = un.Name;
                res[i][1] = un.Address;
                res[i][2] = Math.Round(rate,2).ToString();
                res[i][3] = University.professions[un.ProfId.ToString()].ToString();
                i++;
            }
            return res;
        }


        /// <summary>
        /// Возвращение таблицы со всеми рейтингами. Нужно понять что писать в "план обучения"
        /// </summary>
        /// <returns>Массив массивов строк где 1 элемент сопоставляется 1 клетке таблицы</returns>
        public string[][] GetFullInfo()
        {
            int i = 1;
            string[][] res = new string[universities.Count + 1][];
            res[0] = new string[] { "ID", "Имя", "Адрес", "ARWU", "THE", "USNews", "Общий рейтинг", "План Обучения", "Специальность" };
            foreach (University un in universities)
            {
                //рейтингов может и не быть
                //double rate = (un.USNews.Rate() + un.THE.Rate() + un.ARWU.Rate()) / 3;
                double rate = 0;
                //ошибка
                res[i] = new String[9];
                res[i][0] = un.ID.ToString();
                res[i][1] = un.Name;
                res[i][2] = un.Address;
                //аналогично
                //res[i][3] = un.ARWU.Rate().ToString();
                res[i][3] = (un.ARWU!=null) ? Math.Round(un.ARWU.Rate(),2).ToString():"0";
                //здаесь наверно имелось в виду THE
                //res[i][4] = un.ARWU.Rate().ToString();
                res[i][4] = (un.THE != null) ? Math.Round(un.THE.Rate(),2).ToString() : "0";
                //res[i][5] = un.USNews.Rate().ToString();
                res[i][5] = (un.USNews != null) ? Math.Round(un.USNews.Rate(),2).ToString() : "0";
                rate = (Convert.ToDouble(res[i][3]) + Convert.ToDouble(res[i][4]) + Convert.ToDouble(res[i][5]))/3.0;
                res[i][6] = Math.Round(rate,2).ToString();
                //res[i][7] = un.GetStudPlanByUniversity();
                //нулевая профессия - исправить - это заплатка 
                res[i][8] =(un.ProfId!=0)? University.professions[un.ProfId.ToString()].ToString():"Programmer";
                //забыл увеличить счетчик
                i++;
            }
            return res;
        }
        /// <summary>
        /// Возвращение таблицы с рейтингами предметов
        /// </summary>
        /// <returns>Массив массивов строк где 1 элемент сопоставляется 1 клетке таблицы</returns>
        public string[][] ViewSubjectsGeneral()
        {
            int i = 1;
            string[][] res = new string[subjects.Count + 1][];
            res[0] = new string[] { "Имя", "Количество часов", "Вид занятий", "Обобщенный рейтинг" };
            foreach (Subject sub in subjects)
            {
                res[i] = new string[4];
                res[i][0] = sub.Name;
                res[i][1] = sub.Hours.ToString();
                res[i][2] = (string)Subject.lessons[sub.LessonID.ToString()];
                double rating = sub.SubRating();
                res[i][3] = rating.ToString("0.00");
                i++;
            }
            return res;
        }
        /// <summary>
        /// Возвращение массива всех видов отчетности.
        /// </summary>
        /// <returns>Массив строк с названиями видов отчетности</returns>
        public string[] GetAllEval()
        {
            string[] res = new string[SubjectDetails.evals.Count];
            int i = 0;
            foreach (string val in SubjectDetails.evals.Values)
            {
                res[i] = val;
                i++;
            }
            return res;
        }

        /// <summary>
        /// Возвращение массива всех спецаиальностей.
        /// </summary>
        /// <returns>Массив строк с названиями всех специальностей</returns>
        public string[] GetAllProfessions()
        {
            string[] res = new string[University.professions.Count];
            int i = 0;
            foreach (string val in University.professions.Values)
            {
                res[i] = val;
                i++;
            }
            return res;
        }


        /// <summary>
        /// Возвращение массива всех видов занятий. 
        /// </summary>
        /// <returns>Массив строк с названиями всех видов занятий</returns>
        public string[] GetAllLessonsTypes()
        {
            string[] res = new string[Subject.lessons.Count];
            int i = 0;
            foreach (string val in Subject.lessons.Values)
            {
                res[i] = val;
                i++;
            }
            return res;
        }

        /// <summary>
        /// Возвращение массива всех названий предметов.
        /// </summary>
        /// <returns>Массив строк со всеми названиями предметов</returns>
        public string[] GetAllSubjectNames()
        {
            string[] res = new string[subjects.Count];
            int i = 0;
            foreach (Subject sub in subjects)
            {
                res[i] = sub.Name;
                i++;
            }
            return res;
        }

        /// <summary>
        /// Возвращает объект - университет по ID
        /// </summary>
        /// <param name="id"> id университета</param>
        /// <returns> Университет, соответствующий id</returns>
        public University FindUniversityByID(int id)
        {
            foreach (University un in universities)
            {
                if (un.ID == id)
                    return un;
            }
            throw new Exception("Такого универа нету");
        }


        /// <summary>
        /// Добавление Университета в Базу данных
        /// </summary>
        /// <param name="name">Название университета</param>
        /// <param name="adress">Адрес университета</param>
        public void AddUniversity(string name, string adress)
        {

            universities.Add(new University(name, adress, getUniversityID()));
            //throw new System.NotImplementedException();
        }

        /// <summary>
        /// Добавление предмета в БазуДанных.
        /// </summary>
        /// <param name="name">Имя предмета</param>
        /// <param name="CountOfHours">Число часов</param>
        /// <param name="typeOfSubject">Вид занятий</param>
        public void AddSubject(string name, string CountOfHours, string typeOfSubject)
        {
            subjects.Add(new Subject(name, Int16.Parse(CountOfHours),/*subject.Count*/ GenerateSubjID(), keyFromValue(typeOfSubject, lessonTypes)));
        }

        /// <summary>
        /// Создание нового идентификационного номера для предмета(для сохранения в файл)
        /// </summary>
        /// <returns>ID предмета</returns>
        public int GenerateSubjID()
        {
            int n = 0;
            foreach (Subject s in subjects)
            {
                if (s.ID >= n)
                    n = s.ID + 1;
            }
            return n;
        }

        /// <summary>
        /// Добавление нового типа занятий.
        /// </summary>
        /// <param name="value"> Название типа занятий</param>
        public void AddLessonTypes(string value)
        {
            List<String> temp = new List<String>(GetAllLessonsTypes());
            if (!temp.Contains(value))
            {
                int id = generateIdInHashTable(lessonTypes);
                //lessonTypes.Add(id.ToString(), lessonTypes);
                lessonTypes.Add(id.ToString(), value);
            }
        }

        /// <summary>
        /// Добавление нового вида затчетности.
        /// </summary>
        /// <param name="value">Название зачетности</param>
        public void AddEvalMethod(string value)
        {
            List<String> temp = new List<String>(GetAllEval());
            if (!temp.Contains(value))
            {
                int id = generateIdInHashTable(evalMethods);
                this.evalMethods.Add(id.ToString(), value);
            }
        }

        /// <summary>
        /// Добавление новой специальности.
        /// </summary>
        /// <param name="value">Название специальности</param>
        public void AddProfession(string value)
        {
            List<String> temp = new List<String>(GetAllProfessions());
            if (!temp.Contains(value))
            {
                int id = generateIdInHashTable(professions);
                this.professions.Add(id.ToString(), value);
            }
        }

        /// <summary>
        /// Добавление Студ плана Университету
        /// </summary>
        /// <param name="univ">Университет, которму добавляем</param>
        /// <param name="plan">План, который добавляем</param>
        public void AddStudPlan(University univ, StudPlan plan)
        {
            univ.studPlan = plan;
        }

        ///<summary>
        ///Генерация нового ID для университета
        ///</summary>
        private int getUniversityID()
        {   
            int n = 0;
            //бракую
            foreach (University un in universities)
            {
                if (/*un.ID == n*/un.ID>=n)
                {
                    n = un.ID + 1;
                }
//                 else
//                 {
//                     return n;       
//                 }                
            }
            return n;
        }

        /// <summary>
        /// Генерация нового ID для хеш-таблицы
        /// </summary>
        /// <param name="table">Хеш-таблица</param>
        /// <returns>Новый ID</returns>
        private int generateIdInHashTable( Hashtable table)
        {

            int n = 0;
            foreach (String key in table.Keys)
            {
                if (Convert.ToInt32(key) >= n)
                {
                    n = Convert.ToInt32(key) + 1;
                }
            }
            return n;
        }

        /// <summary>
        /// Получение значения ключа хеш-таблицы по значению
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="table">Хеш-таблица</param>
        /// <returns>номер ключа</returns>
        public static int keyFromValue(String value, Hashtable table)
        {
            int key = 0;
            foreach (String ie in table.Keys)
            {
                if (table[ie].Equals(value))
                {
                    key = Convert.ToInt32(ie);
                }
            }
            return key;
        }

        /// <summary>
        /// Удаление университета из списка
        /// </summary>
        /// <param name="univ">Удаляемый университет</param>
        public void RemoveUniversity(University univ)
        {
            universities.Remove(univ);
        }

        /// <summary>
        /// Полная информация о специальностях
        /// </summary>
        /// <param name="profname">Наименование специальности</param>
        /// <returns>матрицу строк для удобного отображения</returns>
        public String[][] ProfessionFullInfo(String profname)
        {
            List<String[]> res = new List<String[]>();
            String[] temp = new String[1];
            temp[0] = profname;
            res.Add(temp);
            foreach (University u in universities)
            {
                if (professions[u.ProfId.ToString()].Equals(profname))
                    res.AddRange(u.GetUniversityInfo());
            }
            return res.ToArray();
        }

        /// <summary>
        /// Сохранение информации об университетах в файл
        /// </summary>
        /// <param name="fileName">Полное имя файла</param>
        /// <param name="univs">Список университетов</param>
        public void SaveUniversityInfoTofile(String fileName, List<University> univs)
        {
            List<String> ls = new List<String>();
            ls.Add(UNIVERSITY_INFO_HEADER);
            foreach (University u in univs)
            {
                String[][] info = u.GetUniversityInfo();
                String uname=info[0][0];
                String arwuval = info[1][1];
                String theval = info[2][1];
                String usval = info[3][1];
                String semester="1";
                int i = 4;
                while(i < info.Length)
                {
                    if (info[i].Length==1)
                    {
                        semester = info[i][0].Substring(9);
                        i++;
                    }
                    else
                    {
                        ls.Add(uname + ";" + arwuval + ";" + theval + ";" + usval + ";" + semester + ";" + info[i][0] + ";" + info[i][1] + ";" + info[i][2] + ";" + info[i][3]);
                    }
                    i++;
                }
                File.WriteAllLines(fileName, ls.ToArray(), Encoding.GetEncoding("windows-1251"));
            }
        }
        /// <summary>
        /// Сохранение информации о специальностях в файл
        /// </summary>
        /// <param name="fileName">Полное имя файла</param>
        /// <param name="profs">Список специальностей</param>
        public void SaveProfessionsToFile(String fileName, List<String> profs)
        {
            List<String> ls = new List<String>();
            ls.Add(PROFESSION_INFO_HEADER);
            foreach (String p in profs)
            {
                foreach (University u in universities)
                {
                    if (professions[u.ProfId.ToString()].Equals(p))
                    {
                        String[][] info = u.GetUniversityInfo();
                        String uname = info[0][0];
                        String arwuval = info[1][1];
                        String theval = info[2][1];
                        String usval = info[3][1];
                        String semester = "1";
                        int i = 4;
                        while (i < info.Length)
                        {
                            if (info[i].Length == 1)
                            {
                                semester = info[i][0].Substring(9);
                                i++;
                            }
                            else
                            {
                                ls.Add(p+";"+uname + ";" + arwuval + ";" + theval + ";" + usval + ";" + semester + ";" + info[i][0] + ";" + info[i][1] + ";" + info[i][2] + ";" + info[i][3]);
                            }
                            i++;
                        }
                    }
                }
            }
            File.WriteAllLines(fileName, ls.ToArray(), Encoding.GetEncoding("windows-1251"));
        }

        /// <summary>
        /// Сохранение подробной информации о предметах в файл
        /// </summary>
        /// <param name="fileName">Полное имя файла</param>
        /// <param name="subs">Список предметов</param>
        public void SaveSubjectsToFile(string fileName, List<Subject> subs)
        {
            List<String> ls = new List<String>();
            ls.Add(SUBJECTS_INFO_HEADER);
            foreach (Subject s in subs)
            {
                String[][] info = s.FullInfo();
                String sname = info[0][0];
                String uname = "";
                String arwuval = "";
                String theval = "";
                String usval = "";
                int i = 1;
                while (i < info.Length)
                {
                    if (info[i].Length == 1)
                    {
                        uname = info[i][0];
                        arwuval = info[i + 1][1];
                        theval = info[i + 2][1];
                        usval = info[i + 3][1];
                        i += 4;
                    }
                    else
                    {
                        ls.Add(sname + ";" + uname + ";" + arwuval + ";" + theval + ";" + usval + ";" + info[i][0] + ";" + info[i][1]);
                    }
                    i++;
                }
            }
            File.WriteAllLines(fileName, ls.ToArray(), Encoding.GetEncoding("windows-1251"));
        }
    }
}
