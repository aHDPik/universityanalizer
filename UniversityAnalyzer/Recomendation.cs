﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Форма для отображения рекомендации для программы выбранному университету
    /// </summary>
    public partial class Recomendation : Form
    {
        /// <summary>
        /// База данных
        /// </summary>
        public DataBase db;
        /// <summary>
        /// Университет, для которого происходит выдача рекомендаций.
        /// </summary>
        University univer;
        /// <summary>
        /// Конструктор
        /// </summary>
        public Recomendation()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Метод, выполняющийся при загрузке окна выдачи рекомендаций.
        /// </summary>
        private void Recomendation_Load(object sender, EventArgs e)
        {
            foreach (University un in db.universities)
            { univers.Items.Add(un.Name); }
        }
        /// <summary>
        /// Метод, выполняющийся при нажатии на кнопку. Отвечает за отображение рекомендации в textbox.
        /// </summary>
        private void button_Click(object sender, EventArgs e)
        {
            if (univers.SelectedItem != null)
            {
                foreach (University univ in db.universities)
                {
                    if (univers.SelectedItem.Equals(univ.Name))
                    { univer = univ; }
                }
                University.Recommend rec = univer.Recommendation();
                if (rec.newSubs.Count > 0)
                {
                    recom.Text = "Вы выбрали университет " + rec.name + "." + "\r\n";
                    recom.Text += "После анализа учебного плана университета " + rec.name + " было выявлено, что суммарный рейтинг предметов составляет " + rec.totalRating + "." + "\r\n";
                    recom.Text += "Некоторые предметы имеют низкий рейтинг." + "\r\n" + "Мы рекомендуем:" + "\r\n";
                    for (int i = 0; i < rec.oldSubs.Count; i++)
                    {
                        recom.Text += "Заменить предмет " + rec.oldSubs[i] + " на предмет " + rec.newSubs[i] + "." + "\r\n";
                    }
                    recom.Text += "После замены суммарный рейтинг предметов составит " + rec.newRating + "." + "\r\n"; //Вопрос. Можем ли мы выводить несколько рекомендаций (для разных универов) подряд? Или перед каждой новой нужно очищать textbox?
                }
                else
                {
                    recom.Text = "Поздравляем, ваш университет не требует изменений в его плане!";
                }
            }
            
        }

        private void recom_TextChanged(object sender, EventArgs e)
        {
            if (recom.Text.Equals(""))
            {
                saveBtn.Enabled = false;
            }
            else if (!saveBtn.Enabled)
            {
                saveBtn.Enabled = true;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (savedial.ShowDialog()==DialogResult.OK)
            {
                File.WriteAllText(savedial.FileName, recom.Text, Encoding.GetEncoding("windows-1251"));
            }
        }
    }
}
