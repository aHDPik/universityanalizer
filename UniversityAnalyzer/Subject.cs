﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс предмет
    /// </summary>
    public class Subject
    {
        /// <summary>
        /// Статический список университетов для получения общего рейтинга предметов
        /// </summary>
        public static List<University> univers;
        /// <summary>
        /// Статическое поле видов занятий в виде хеш-таблицы, где ид вида - ключ, название вида - значение
        /// </summary>
        public static Hashtable lessons;
        /// <summary>
        /// Наименование предмета
        /// </summary>
        private String name;
        /// <summary>
        /// Количество часов, отведенных на семестр
        /// </summary>
        private int hours;
        /// <summary>
        /// Идентификационный номер предмета
        /// </summary>
        private int id;
        /// <summary>
        /// идентификационный номер вида занятий
        /// </summary>
        private int lessonId;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Наименование предмета</param>
        /// <param name="hours">Количество часов, отведенных на семестр</param>
        /// <param name="id">Идентификационный номер предмета</param>
        /// <param name="lid">идентификационный номер вида занятий</param>
        public Subject(String name, int hours, int id, int lid)
        {
            this.name = name;
            this.hours = hours;
            this.id = id;
            lessonId = lid;
        }

        /// <summary>
        /// Свойство выдающее строку с информацией о данном предмете
        /// </summary>
        public String Info
        {
            get { return "Subject name: " + name + ", total hours: " + Convert.ToString(hours) + ", Lesson Type: " + lessons[lessonId.ToString()]; }
        }

        /// <summary>
        /// Свойство, выдающее Идентификационный номер предмета
        /// только чтение
        /// </summary>
        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// Свойство, выдающее количество часов данного предмета
        /// только чтение
        /// </summary>
        public int Hours
        {
            get { return hours; }
        }

        /// <summary>
        /// Свойство, выдающее Идентификационный номер вида занятий данного предмета
        /// только чтение
        /// </summary>
        public int LessonID
        {
            get { return lessonId; }
        }

        /// <summary>
        /// Строка с информацией о предмете для записи в файл
        /// </summary>
        public String ToFile
        {
            get { return id.ToString() + ";" + name + ";" + hours.ToString() + ";" + lessonId + System.Environment.NewLine; }
        }

        /// <summary>
        /// Свойство выдающее навзание предмета
        /// </summary>
        public String Name
        {
            get { return name; }
        }
        /// <summary>
        /// Метод подсчитывающий рейтинг предмета
        /// </summary>
        /// <returns>Действительное число от 0 до 100</returns>
        public double SubRating()
        { 
            double sum=0;
            // Количество университетов
            int i=0;
            // Количество необязательных предметов
            int k=0;
            double weigth;
              
            foreach (University u in univers)
            {
                weigth = 0;
                i++;
                foreach (SubjectDetails sd in u.studPlan.Subjects)
                {
                    if (sd.Subj == this && sd.Mandatory == "true")
                    {
                        weigth = u.Rate();
                        break;
                    }
                    if (sd.Subj == this && sd.Mandatory == "false")
                    {
                        foreach (SubjectDetails sd1 in u.studPlan.Subjects)
                        {
                            if (sd1.Mandatory == "false") k++;
                        }
                        weigth = u.Rate() / k;
                        break;
                    }
                }
                sum = sum + weigth;
            }
            sum = sum / i;
            return sum;
        }

        /// <summary>
        /// Полная информация о предмете
        /// </summary>
        /// <returns>Матрица строк для удобного вывода в виде таблицы</returns>
        public String[][] FullInfo()
        {
            List<String[]> res = new List<String[]>();
            String[] temp = new String[1];
            temp[0] = Name;
            res.Add(temp);
            foreach (University u in univers)
            {
                bool edited = false;
                temp = new String[1];
                temp[0] = u.Name;
                res.Add(temp);
                temp = new String[2];
                temp[0] = "ARWU";
                temp[1] = u.ARWU.Rate().ToString("0.00");
                res.Add(temp);
                temp = new String[2];
                temp[0] = "THE";
                temp[1] = u.THE.Rate().ToString("0.00");
                res.Add(temp);
                temp = new String[2];
                temp[0] = "USNews";
                temp[1] = u.USNews.Rate().ToString("0.00");
                res.Add(temp);
                temp = new String[2];
                temp[0] = "Семестр";
                temp[1] = "Форма отчетности";
                res.Add(temp);
                foreach (SubjectDetails sub in u.Plan.Subjects)
                {
                    if (sub.Subj==this)
                    {
                        edited = true;
                        temp = new String[2];
                        temp[0] = sub.Semester.ToString();
                        temp[1] = sub.Eval;
                        res.Add(temp);
                    }
                }
                if (!edited)
                {
                    res.RemoveRange(res.Count - 5, 5);
                }
            }
            return res.ToArray();
        }
    }
}
