﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityAnalyzer
{
    static public class Clone
    {
        [ThreadStatic]
        static Dictionary<object, object> Cache;
        /// <summary>
        /// указатель на метод MemberwiseClone присущий всем объектом в CLR, но закрытый protected модификатором
        /// </summary>
        public static readonly new System.Converter<object, object> MemberwiseClone =
        System.Delegate.CreateDelegate
        (
            typeof(System.Converter<object, object>),
            null,
            typeof(object).GetMethod("MemberwiseClone", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
        )
        as System.Converter<object, object>;

        /// <summary>
        /// функция создания глубокой копии объекта
        /// </summary>
        /// <param name="obj">клонируемый объект</param>
        /// <returns>копия объекта</returns>
        public static object DeepClone(object obj)
        {
            Dictionary<object, object> cache = Clone.Cache ?? (Clone.Cache = new Dictionary<object, object>());
            object clone = DeepClone(obj, cache);
            cache.Clear();
            return clone;
        }
        static object DeepClone(object obj, Dictionary<object, object> cache)
        {
            object clone;
            if (object.ReferenceEquals(obj, null)) return null;                     //если объект ~ null, то и клон его - null
            else if (cache.TryGetValue(obj, out clone)) return clone;               //если ссылка цикличная - то повторно ее не клонируем
            else
            {
                if (obj is string) return string.Copy(obj as string);               //если тип - строка то производим полное копирование
                else if (obj is System.Array)                                       //если объект - массив, необходимо поочередно глубококлонировать его элементы
                {
                    System.Array array = (obj as System.Array).Clone() as System.Array; //клонируем массив
                    cache.Add(obj, array);                                              //добавляем в словарь клонированных ссылок
                    if (array.GetType().GetElementType().IsPrimitive) return array;
                    else
                    {
                        int ei, i;
                        if (array.Rank == 1)                                            //для ускорения различаем массивы по числу размерностей
                        {
                            ei = array.GetUpperBound(0) + 1;
                            for (i = array.GetLowerBound(0); i < ei; i++)
                                array.SetValue(DeepClone(array.GetValue(i), cache), i);
                            return array;
                        }
                        else
                        {
                            int rank = array.Rank;
                            int[] lower = new int[rank];
                            int[] upper = new int[rank];
                            int[] indexes = new int[rank];
                            for (i = 0; i < rank; i++)
                            {
                                indexes[i] = lower[i] = array.GetLowerBound(i);
                                upper[i] = array.GetUpperBound(i);
                            }
                            for (rank--; ; rank--)
                            {
                                for (; indexes[rank] <= upper[rank]; indexes[rank]++)
                                    array.SetValue(DeepClone(array.GetValue(indexes), cache), indexes);
                                do { if (rank == 0) return array; }
                                while (indexes[--rank] == upper[rank]);
                                indexes[rank]++;
                                while (++rank < array.Rank)
                                    indexes[rank] = lower[rank];
                            }
                        }
                    }
                }
                else // объект не null, не строка и не массив
                {
                    Type tyobj = obj.GetType();             //узнаем тип объекта
                    if (tyobj.IsPrimitive) return obj;      //если тип является примитивом (int, double и т.п.) то нет смысла копаться в его внутренностях
                    else
                    {
                        clone = MemberwiseClone(obj);       //клонируем объект
                        cache.Add(obj, clone);              //добавляем в словарь клонированных ссылок
                        //клонируем все поля объекта
                        foreach (var field in tyobj.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic))
                            field.SetValue(clone, DeepClone(field.GetValue(clone), cache));
                        return clone;
                    }
                }
            }
        }
    }
}
