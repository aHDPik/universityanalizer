﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    //коментарии!!!
    /// <summary>
    /// Форма для показа информации об университетах
    /// </summary>
    public partial class InfoForm : Form
    {
        private string[][] mas;
        private Bitmap buf;
        private int y;
        //зачем нужна эта переменная я не понял
        private int y_min=0;
        private int y_max;
        private DataBase db;
        private List<University> listUn = new List<University>();
        private const int OFFSET_X = 15;
        private const int OFFSET_Y = 70;
        private const int HEIGHT = 30;
        private bool draw = true;
        /// <summary>
        /// режим информации университетов
        /// </summary>
        public const int UNIVRISITIES = 1;
        /// <summary>
        /// режим информации специальностей 
        /// </summary>
        public const int PROFESSIONS = 2;
        /// <summary>
        /// режим информации предметов
        /// </summary>
        public const int SUBJECTS = 3;
        private int mode;

        /// <summary>
        /// Инициализируем компоненты
        /// </summary>
        public InfoForm()
        {
            InitializeComponent();
            MouseWheel += panel1_MouseDown;
        }

        /// <summary>
        /// Конструируем наше окно.
        /// </summary>
        /// <param name="db">База Данных, с которой работаем</param>
        /// <param name="mode">Режим показа информации</param>
        /// <returns>готовое окно</returns>
        public DialogResult ShowDialog(DataBase db, int mode)
        {
            this.db = db;
            this.mode = mode;
            listAllUniv.View = View.Details;
            listChosUniv.View = View.Details;
            //y = this.canvas.Location.Y;
            y = 0;
            if (mode == UNIVRISITIES)
            {
                allUniv.Text = "Все университеты";
                listChosUniv.Columns.Clear();
                listChosUniv.Columns.Add("Название университета");
                listAllUniv.Columns.Add("Название университета");
                foreach (University u in db.Universities)
                {
                    listAllUniv.Items.Add(u.Name);
                    listAllUniv.Items[listAllUniv.Items.Count - 1].Tag = u;

                }
            }
            else if (mode == PROFESSIONS)
            {
                allUniv.Text = "Все специальности";
                listChosUniv.Columns.Clear();
                listChosUniv.Columns.Add("Название специальности");
                listAllUniv.Columns.Add("Название специальности");
                Text = "Информация о профессиях";
                foreach (String s in db.GetAllProfessions())
                {
                    listAllUniv.Items.Add(s);
                }
            }
            else
            {
                allUniv.Text = "Все предметы";
                listChosUniv.Columns.Clear();
                listChosUniv.Columns.Add("Название предмета");
                listAllUniv.Columns.Add("Название предмета");
                Text = "Информация о предметах";
                foreach (Subject sub in SubjectDetails.allSubjects)
                {
                    listAllUniv.Items.Add(sub.Name);
                    listAllUniv.Items[listAllUniv.Items.Count - 1].Tag = sub;
                }
            }
            listAllUniv.Columns[0].Width = listAllUniv.Width;
            listChosUniv.Columns[0].Width = listAllUniv.Width;
            return ShowDialog();
        }

        /// <summary>
        /// Добавялем выбранные университеты 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttnAdd_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listAllUniv.SelectedItems)
            {
                listChosUniv.Items.Add(lvi.Text);
                listChosUniv.Items[listChosUniv.Items.Count - 1].Tag = lvi.Tag;
                listAllUniv.Items.Remove(lvi);
            }
        }

        /// <summary>
        /// Удоляем выбранные университеты
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listChosUniv.SelectedItems)
            {
                listAllUniv.Items.Add(lvi.Text);
                listAllUniv.Items[listAllUniv.Items.Count - 1].Tag = lvi.Tag;
                listChosUniv.Items.Remove(lvi);
            }
        }

        /// <summary>
        /// Переключаемся на Panel и обратно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (buttonShow.Tag.Equals("1"))
                buttonShow.Tag = "";
            else buttonShow.Tag = "1";
            if (draw)
            {
                this.buttnAdd.Visible = false;
                this.buttonClear.Visible = false;
                this.buttonShow.Text = "Выбрать";
                this.allUniv.Visible = false;
                this.selectedUniv.Visible = false;
                this.listAllUniv.Visible = false;
                this.listChosUniv.Visible = false;
                saveBtn.Visible = true;
                y = canvas.Location.Y;
                mas = FillMas();
                this.Height = (mas.Length * HEIGHT + OFFSET_Y < 600) ?  mas.Length * HEIGHT + OFFSET_Y : 600;
                this.canvas.Height = this.Height - OFFSET_Y;
                this.canvas.Width = this.Width - OFFSET_X;
                //mas = db.GetFullInfo();
                buf = new Bitmap(canvas.Width, canvas.Height);
                Graphics bufG = Graphics.FromImage(buf);
                //DrawTable(bufG);
                y_max=mas.Length*HEIGHT;
                TableDrawing.DrawTable(bufG, y, 0, y_max, HEIGHT, canvas.Width, canvas.Height, mas);
                this.canvas.Invalidate();
                this.canvas.Visible = true;
                draw = false;
            }
            else
            {
                this.Height = this.MinimumSize.Height;
                this.Width = this.MinimumSize.Width;
                this.canvas.Visible = false;
                this.buttnAdd.Visible = true;
                this.buttonClear.Visible = true;
                this.buttonShow.Text = "Показать";
                this.allUniv.Visible = true;
                this.selectedUniv.Visible = true;
                this.listAllUniv.Visible = true;
                this.listChosUniv.Visible = true;
                saveBtn.Visible = false;
                draw = true;
            }
        }

        /// <summary>
        /// Событ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onPaint(object sender, PaintEventArgs e)
        {
            if (this.canvas.Visible)
                 e.Graphics.DrawImage(buf,0,0);
            /*Graphics g = e.Graphics; // Сохранить Graphics
            if (this.panel1.Visible)
                DrawTable(g); */
        }

        /// <summary>
        /// Рисуем таблицу
        /// </summary>
        /// <param name="g">Рисуем на g</param>
        void DrawTable(Graphics g) 
        {
            //y_min = this.canvas.Location.Y;  
            //создается очень много объектов абсолютно одинаковых
            y_min = 0;
            int y_width = this.canvas.Height;
            int x0 = this.canvas.Location.X;
            int y0 = - ((y-y_min) % HEIGHT);
            //что такое 4
            int width = this.canvas.Width - 4;     
            Pen p1 = new Pen(Color.Black, 2f);
            SolidBrush b1 = new SolidBrush(Color.Gray);
            SolidBrush b2 = new SolidBrush(Color.Black);
            Font drawFont = new Font("Microsoft Sans Serif", 16);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.FillRectangle(new SolidBrush(Color.White), this.DisplayRectangle);
            //g.DrawRectangle(p1, this.DisplayRectangle);
            y_max = HEIGHT * mas.Length;
            //что такое 1
            for (int i = 0; (i < y_width / HEIGHT + 1) && i < mas.Length; i++)
            {
                //что такое 2
                x0 = this.canvas.Location.X + 2;
                for (int j = 0; j < mas[(y - y_min) / HEIGHT + i].Length; j++)
                {
                   //прокомментировать каждую строчку
                    Rectangle rec = new Rectangle(x0, y0, width / mas[(y - y_min) / HEIGHT + i].Length, HEIGHT);
                    //g.FillRectangle(b2, x0, y0, (float)(Width / mas[i].Length), hiight);
                    g.DrawRectangle(p1, rec);
                    g.DrawString(mas[(y - y_min) / HEIGHT + i][j], drawFont, b2, rec, stringFormat);
                    x0 = x0 + (width / mas[(y - y_min) / HEIGHT + i].Length);
                }
                y0 = y0 + HEIGHT;
            }
        }

        /// <summary>
        /// Таблица заполненная информацией о всех выбранных вузах  
        /// </summary>
        /// <returns></returns>
        private string[][] FillMas()
        {
            //не понял зачем был нужен 3мерный массив ну да ладно
            string[][][] bigMas = new string[this.listChosUniv.Items.Count][][];
            int i = 0;
            int sum = 0;
            foreach (ListViewItem lvi in listChosUniv.Items)
            {
                if (mode == UNIVRISITIES)
                    bigMas[i] = ((University)lvi.Tag).GetUniversityInfo(); //Осталось раскомментировать при слияние всего кода
                else if (mode == PROFESSIONS)
                    bigMas[i] = db.ProfessionFullInfo(lvi.Text);
                else
                    bigMas[i] = (lvi.Tag as Subject).FullInfo();
                //bigMas[i] = db.GetFullInfo();
                sum += bigMas[i].Length;
                i++;
            }
            String[][] mas = new string[sum][];
            int a = 0;
            for (i = 0; i < bigMas.Length; i++)
            {
                for (int j = 0; j < bigMas[i].Length; j++)
                {
                    mas[a] = new string[bigMas[i][j].Length];
                    for (int k = 0; k < bigMas[i][j].Length; k++ )
                    {
                        mas[a][k] = bigMas[i][j][k];
                    }
                    a++;
                }
            }
                return mas;
        }
        /// <summary>
        /// Изменение размера формы, влечет изменение размера панели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InfoForm_ResizeEnd(object sender, EventArgs e)
        {
            this.canvas.Height = this.Height - OFFSET_Y;
            this.canvas.Width = this.Width - OFFSET_X;
            if (this.canvas.Height > y_max)
                //y = this.canvas.Location.Y;
                y = y_min;
            buf = new Bitmap(canvas.Width, canvas.Height);
            Graphics bufG = Graphics.FromImage(buf);
            //DrawTable(bufG);
            if(!buttonShow.Tag.Equals("1"))
                TableDrawing.DrawTable(bufG, y, 0, y_max, HEIGHT, canvas.Width, canvas.Height, mas);
            this.canvas.Invalidate();
        }
        /// <summary>
        /// Скроллинг при нажатой правой кнопки мыши
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (y_max > this.canvas.Height)
            {
                int yprev = y;
                if (((y - e.Delta / 12) > y_min) && ((y - e.Delta / 12 + this.canvas.Height) < y_max))
                {
                    y -= e.Delta / 12;
                }
                else
                {
                    y = ((y - e.Delta / 12) <= y_min) ? y_min : y_max - this.canvas.Height;
                }
                if (yprev != y)
                {
                    buf = new Bitmap(canvas.Width, canvas.Height);
                    Graphics bufG = Graphics.FromImage(buf);
                    //DrawTable(bufG);
                    TableDrawing.DrawTable(bufG, y, 0, y_max, HEIGHT, canvas.Width, canvas.Height, mas);
                    this.canvas.Invalidate();
                }
            }
        }
        /// <summary>
        /// Скроллинг
        /// </summary>
        /// <param name="sender">Канва</param>
        /// <param name="e">Аргументы события Мыши</param>
        private void panel1_MouseClick_1(object sender, MouseEventArgs e)
        {
            if (((y + e.Delta) > y_min) && ((y + e.Delta + this.canvas.Height) < y_max))
            {
                y += e.Delta;
                buf = new Bitmap(canvas.Width, canvas.Height);
                Graphics bufG = Graphics.FromImage(buf);
                //DrawTable(bufG);
                TableDrawing.DrawTable(bufG, y, 0, y_max, HEIGHT, canvas.Width, canvas.Height, mas);
                this.canvas.Invalidate();
            }
        }
        /// <summary>
        /// скроллинг по клику (пока оставлю так)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onClick(object sender, EventArgs e)
        {
            
            if ((y + 10) > y_min && (y + 10 + this.canvas.Height) < y_max)
            {
                y += 10;
                buf = new Bitmap(canvas.Width, canvas.Height);
                Graphics bufG = Graphics.FromImage(buf);
                //DrawTable(bufG);
                TableDrawing.DrawTable(bufG, y, 0, y_max, HEIGHT, canvas.Width, canvas.Height, mas);
                this.canvas.Invalidate();
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (savedial.ShowDialog()==DialogResult.OK)
            {
                if (mode==UNIVRISITIES)
                {
                    List<University> lst = new List<University>();
                    foreach (ListViewItem lvi in listChosUniv.Items)
                    {
                        lst.Add(lvi.Tag as University);
                    }
                    db.SaveUniversityInfoTofile(savedial.FileName, lst);
                }
                else if (mode == PROFESSIONS)
                {
                    List<String> lst = new List<String>();
                    foreach (ListViewItem lvi in listChosUniv.Items)
                    {
                        lst.Add(lvi.Text);
                    }
                    db.SaveProfessionsToFile(savedial.FileName, lst);
                }
                else
                {
                    List<Subject> lst = new List<Subject>();
                    foreach (ListViewItem lvi in listChosUniv.Items)
                    {
                        lst.Add(lvi.Tag as Subject);
                    }
                    db.SaveSubjectsToFile(savedial.FileName, lst);
                }
            }
        }


    }

}
