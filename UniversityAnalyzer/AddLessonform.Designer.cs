﻿namespace UniversityAnalyzer
{
    partial class AddLessonform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.hoursLabel = new System.Windows.Forms.Label();
            this.hoursNum = new System.Windows.Forms.NumericUpDown();
            this.lessonLabel = new System.Windows.Forms.Label();
            this.lessonSelect = new System.Windows.Forms.ComboBox();
            this.addBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.hoursNum)).BeginInit();
            this.SuspendLayout();
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Location = new System.Drawing.Point(229, 271);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 0;
            this.okBtn.Text = "ОК";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(310, 271);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 0;
            this.cancelBtn.Text = "Отмена";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(12, 25);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(60, 13);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Название:";
            // 
            // nameInput
            // 
            this.nameInput.Location = new System.Drawing.Point(144, 22);
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(160, 20);
            this.nameInput.TabIndex = 2;
            // 
            // hoursLabel
            // 
            this.hoursLabel.AutoSize = true;
            this.hoursLabel.Location = new System.Drawing.Point(12, 57);
            this.hoursLabel.Name = "hoursLabel";
            this.hoursLabel.Size = new System.Drawing.Size(101, 13);
            this.hoursLabel.TabIndex = 1;
            this.hoursLabel.Text = "Количество часов:";
            // 
            // hoursNum
            // 
            this.hoursNum.Location = new System.Drawing.Point(144, 55);
            this.hoursNum.Name = "hoursNum";
            this.hoursNum.Size = new System.Drawing.Size(160, 20);
            this.hoursNum.TabIndex = 3;
            // 
            // lessonLabel
            // 
            this.lessonLabel.AutoSize = true;
            this.lessonLabel.Location = new System.Drawing.Point(12, 87);
            this.lessonLabel.Name = "lessonLabel";
            this.lessonLabel.Size = new System.Drawing.Size(73, 13);
            this.lessonLabel.TabIndex = 1;
            this.lessonLabel.Text = "Вид занятий:";
            // 
            // lessonSelect
            // 
            this.lessonSelect.FormattingEnabled = true;
            this.lessonSelect.Location = new System.Drawing.Point(144, 84);
            this.lessonSelect.Name = "lessonSelect";
            this.lessonSelect.Size = new System.Drawing.Size(160, 21);
            this.lessonSelect.TabIndex = 4;
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(310, 84);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 23);
            this.addBtn.TabIndex = 0;
            this.addBtn.Text = "Добавить";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // AddLessonform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 306);
            this.Controls.Add(this.lessonSelect);
            this.Controls.Add(this.hoursNum);
            this.Controls.Add(this.nameInput);
            this.Controls.Add(this.lessonLabel);
            this.Controls.Add(this.hoursLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.okBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddLessonform";
            this.Text = "Добавление тогово предмета";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddLessonform_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.hoursNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.Label hoursLabel;
        private System.Windows.Forms.NumericUpDown hoursNum;
        private System.Windows.Forms.Label lessonLabel;
        private System.Windows.Forms.ComboBox lessonSelect;
        private System.Windows.Forms.Button addBtn;
    }
}