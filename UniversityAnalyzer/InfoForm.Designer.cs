﻿namespace UniversityAnalyzer
{
    partial class InfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listAllUniv = new System.Windows.Forms.ListView();
            this.listChosUniv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttnAdd = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.allUniv = new System.Windows.Forms.Label();
            this.selectedUniv = new System.Windows.Forms.Label();
            this.buttonShow = new System.Windows.Forms.Button();
            this.canvas = new System.Windows.Forms.Panel();
            this.saveBtn = new System.Windows.Forms.Button();
            this.savedial = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // listAllUniv
            // 
            this.listAllUniv.Location = new System.Drawing.Point(12, 44);
            this.listAllUniv.Name = "listAllUniv";
            this.listAllUniv.ShowGroups = false;
            this.listAllUniv.Size = new System.Drawing.Size(208, 159);
            this.listAllUniv.TabIndex = 0;
            this.listAllUniv.UseCompatibleStateImageBehavior = false;
            // 
            // listChosUniv
            // 
            this.listChosUniv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listChosUniv.Location = new System.Drawing.Point(307, 44);
            this.listChosUniv.Name = "listChosUniv";
            this.listChosUniv.Size = new System.Drawing.Size(227, 155);
            this.listChosUniv.TabIndex = 1;
            this.listChosUniv.UseCompatibleStateImageBehavior = false;
            // 
            // buttnAdd
            // 
            this.buttnAdd.Location = new System.Drawing.Point(226, 60);
            this.buttnAdd.Name = "buttnAdd";
            this.buttnAdd.Size = new System.Drawing.Size(75, 23);
            this.buttnAdd.TabIndex = 2;
            this.buttnAdd.Text = "→";
            this.buttnAdd.UseVisualStyleBackColor = true;
            this.buttnAdd.Click += new System.EventHandler(this.buttnAdd_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(226, 118);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 3;
            this.buttonClear.Text = "←";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.button2_Click);
            // 
            // allUniv
            // 
            this.allUniv.AutoSize = true;
            this.allUniv.Location = new System.Drawing.Point(12, 19);
            this.allUniv.Name = "allUniv";
            this.allUniv.Size = new System.Drawing.Size(106, 13);
            this.allUniv.TabIndex = 4;
            this.allUniv.Text = "Все Университеты:";
            // 
            // selectedUniv
            // 
            this.selectedUniv.AutoSize = true;
            this.selectedUniv.Location = new System.Drawing.Point(410, 19);
            this.selectedUniv.Name = "selectedUniv";
            this.selectedUniv.Size = new System.Drawing.Size(87, 13);
            this.selectedUniv.TabIndex = 5;
            this.selectedUniv.Text = "Интересующие:";
            // 
            // buttonShow
            // 
            this.buttonShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShow.Location = new System.Drawing.Point(459, 205);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(75, 23);
            this.buttonShow.TabIndex = 6;
            this.buttonShow.Tag = "1";
            this.buttonShow.Text = "Показать";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.button1_Click);
            // 
            // canvas
            // 
            this.canvas.AutoScroll = true;
            this.canvas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.canvas.Location = new System.Drawing.Point(1, 1);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(548, 226);
            this.canvas.TabIndex = 7;
            this.canvas.Visible = false;
            this.canvas.Click += new System.EventHandler(this.onClick);
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.onPaint);
            this.canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick_1);
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.Location = new System.Drawing.Point(367, 204);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 0;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Visible = false;
            this.saveBtn.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // savedial
            // 
            this.savedial.DefaultExt = "csv";
            this.savedial.Filter = "csv файлы|*.csv";
            this.savedial.Title = "Выберите имя файла";
            // 
            // InfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 234);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.selectedUniv);
            this.Controls.Add(this.allUniv);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttnAdd);
            this.Controls.Add(this.listChosUniv);
            this.Controls.Add(this.listAllUniv);
            this.Controls.Add(this.canvas);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(566, 272);
            this.Name = "InfoForm";
            this.ShowIcon = false;
            this.Text = "Подробная информация об университетах";
            this.ResizeEnd += new System.EventHandler(this.InfoForm_ResizeEnd);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listAllUniv;
        private System.Windows.Forms.ListView listChosUniv;
        private System.Windows.Forms.Button buttnAdd;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label allUniv;
        private System.Windows.Forms.Label selectedUniv;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Panel canvas;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.SaveFileDialog savedial;
    }
}