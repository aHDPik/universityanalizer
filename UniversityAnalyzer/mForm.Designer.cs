﻿namespace UniversityAnalyzer
{
    partial class mForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mForm));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.БазаданныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьНовуюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.правкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.просмотрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вУЗToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aRWUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tHEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSNewsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обобщенныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.подробнаяИнформацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.информацияПоПрофессиямToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.предметToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.подробнаяИнформацияToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.рекомендацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.table = new System.Windows.Forms.ListView();
            this.viewm = new System.Windows.Forms.DataGridView();
            this.рейтингToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.информацияОбУниверситетахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewm)).BeginInit();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.правкаToolStripMenuItem,
            this.просмотрToolStripMenuItem,
            this.рекомендацииToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(736, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.БазаданныхToolStripMenuItem,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // БазаданныхToolStripMenuItem
            // 
            this.БазаданныхToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.создатьНовуюToolStripMenuItem});
            this.БазаданныхToolStripMenuItem.Name = "БазаданныхToolStripMenuItem";
            this.БазаданныхToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.БазаданныхToolStripMenuItem.Text = "База данных";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Enabled = false;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // создатьНовуюToolStripMenuItem
            // 
            this.создатьНовуюToolStripMenuItem.Name = "создатьНовуюToolStripMenuItem";
            this.создатьНовуюToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.создатьНовуюToolStripMenuItem.Text = "Создать новую";
            this.создатьНовуюToolStripMenuItem.Click += new System.EventHandler(this.создатьНовуюToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(138, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // правкаToolStripMenuItem
            // 
            this.правкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.информацияОбУниверситетахToolStripMenuItem});
            this.правкаToolStripMenuItem.Enabled = false;
            this.правкаToolStripMenuItem.Name = "правкаToolStripMenuItem";
            this.правкаToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.правкаToolStripMenuItem.Text = "Правка";
            // 
            // просмотрToolStripMenuItem
            // 
            this.просмотрToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вУЗToolStripMenuItem,
            this.предметToolStripMenuItem});
            this.просмотрToolStripMenuItem.Enabled = false;
            this.просмотрToolStripMenuItem.Name = "просмотрToolStripMenuItem";
            this.просмотрToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.просмотрToolStripMenuItem.Text = "Просмотр";
            // 
            // вУЗToolStripMenuItem
            // 
            this.вУЗToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aRWUToolStripMenuItem,
            this.tHEToolStripMenuItem,
            this.uSNewsToolStripMenuItem,
            this.обобщенныйToolStripMenuItem,
            this.подробнаяИнформацияToolStripMenuItem,
            this.информацияПоПрофессиямToolStripMenuItem});
            this.вУЗToolStripMenuItem.Name = "вУЗToolStripMenuItem";
            this.вУЗToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.вУЗToolStripMenuItem.Text = "ВУЗ";
            // 
            // aRWUToolStripMenuItem
            // 
            this.aRWUToolStripMenuItem.Name = "aRWUToolStripMenuItem";
            this.aRWUToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.aRWUToolStripMenuItem.Tag = "3";
            this.aRWUToolStripMenuItem.Text = "ARWU";
            this.aRWUToolStripMenuItem.Click += new System.EventHandler(this.Rating_Click);
            // 
            // tHEToolStripMenuItem
            // 
            this.tHEToolStripMenuItem.Name = "tHEToolStripMenuItem";
            this.tHEToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.tHEToolStripMenuItem.Tag = "4";
            this.tHEToolStripMenuItem.Text = "THE";
            this.tHEToolStripMenuItem.Click += new System.EventHandler(this.Rating_Click);
            // 
            // uSNewsToolStripMenuItem
            // 
            this.uSNewsToolStripMenuItem.Name = "uSNewsToolStripMenuItem";
            this.uSNewsToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.uSNewsToolStripMenuItem.Tag = "5";
            this.uSNewsToolStripMenuItem.Text = "U.S. News";
            this.uSNewsToolStripMenuItem.Click += new System.EventHandler(this.Rating_Click);
            // 
            // обобщенныйToolStripMenuItem
            // 
            this.обобщенныйToolStripMenuItem.Name = "обобщенныйToolStripMenuItem";
            this.обобщенныйToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.обобщенныйToolStripMenuItem.Tag = "6";
            this.обобщенныйToolStripMenuItem.Text = "Обобщенный";
            this.обобщенныйToolStripMenuItem.Click += new System.EventHandler(this.Rating_Click);
            // 
            // подробнаяИнформацияToolStripMenuItem
            // 
            this.подробнаяИнформацияToolStripMenuItem.Name = "подробнаяИнформацияToolStripMenuItem";
            this.подробнаяИнформацияToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.подробнаяИнформацияToolStripMenuItem.Text = "Подробная информация";
            this.подробнаяИнформацияToolStripMenuItem.Click += new System.EventHandler(this.подробнаяИнформацияToolStripMenuItem_Click);
            // 
            // информацияПоПрофессиямToolStripMenuItem
            // 
            this.информацияПоПрофессиямToolStripMenuItem.Name = "информацияПоПрофессиямToolStripMenuItem";
            this.информацияПоПрофессиямToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.информацияПоПрофессиямToolStripMenuItem.Text = "Информация по профессиям";
            this.информацияПоПрофессиямToolStripMenuItem.Click += new System.EventHandler(this.информацияПоПрофессиямToolStripMenuItem_Click);
            // 
            // предметToolStripMenuItem
            // 
            this.предметToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.подробнаяИнформацияToolStripMenuItem1,
            this.рейтингToolStripMenuItem});
            this.предметToolStripMenuItem.Name = "предметToolStripMenuItem";
            this.предметToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.предметToolStripMenuItem.Tag = "";
            this.предметToolStripMenuItem.Text = "Предмет";
            // 
            // подробнаяИнформацияToolStripMenuItem1
            // 
            this.подробнаяИнформацияToolStripMenuItem1.Name = "подробнаяИнформацияToolStripMenuItem1";
            this.подробнаяИнформацияToolStripMenuItem1.Size = new System.Drawing.Size(211, 22);
            this.подробнаяИнформацияToolStripMenuItem1.Text = "Подробная информация";
            this.подробнаяИнформацияToolStripMenuItem1.Click += new System.EventHandler(this.подробнаяИнформацияToolStripMenuItem1_Click);
            // 
            // рекомендацииToolStripMenuItem
            // 
            this.рекомендацииToolStripMenuItem.Enabled = false;
            this.рекомендацииToolStripMenuItem.Name = "рекомендацииToolStripMenuItem";
            this.рекомендацииToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.рекомендацииToolStripMenuItem.Text = "Рекомендации";
            this.рекомендацииToolStripMenuItem.Click += new System.EventHandler(this.рекомендацииToolStripMenuItem_Click);
            // 
            // table
            // 
            this.table.Location = new System.Drawing.Point(0, 26);
            this.table.Margin = new System.Windows.Forms.Padding(2);
            this.table.Name = "table";
            this.table.Size = new System.Drawing.Size(725, 243);
            this.table.TabIndex = 0;
            this.table.UseCompatibleStateImageBehavior = false;
            this.table.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.table_ColumnClick);
            // 
            // viewm
            // 
            this.viewm.AllowUserToAddRows = false;
            this.viewm.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle1.NullValue = null;
            this.viewm.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.viewm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.viewm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.viewm.Location = new System.Drawing.Point(12, 27);
            this.viewm.Name = "viewm";
            this.viewm.ReadOnly = true;
            this.viewm.Size = new System.Drawing.Size(712, 287);
            this.viewm.TabIndex = 1;
            this.viewm.Visible = false;
            // 
            // рейтингToolStripMenuItem
            // 
            this.рейтингToolStripMenuItem.Name = "рейтингToolStripMenuItem";
            this.рейтингToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.рейтингToolStripMenuItem.Tag = "1";
            this.рейтингToolStripMenuItem.Text = "Рейтинг";
            this.рейтингToolStripMenuItem.Click += new System.EventHandler(this.Rating_Click);
            // 
            // информацияОбУниверситетахToolStripMenuItem
            // 
            this.информацияОбУниверситетахToolStripMenuItem.Name = "информацияОбУниверситетахToolStripMenuItem";
            this.информацияОбУниверситетахToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.информацияОбУниверситетахToolStripMenuItem.Text = "Информация об университетах";
            this.информацияОбУниверситетахToolStripMenuItem.Click += new System.EventHandler(this.правкаToolStripMenuItem_Click);
            // 
            // mForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 326);
            this.Controls.Add(this.table);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mForm";
            this.Text = "Рейтинговый анализатор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mForm_FormClosing);
            this.Load += new System.EventHandler(this.mForm_Load);
            this.Resize += new System.EventHandler(this.mForm_Resize);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem БазаданныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьНовуюToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem правкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem просмотрToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вУЗToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aRWUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tHEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSNewsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обобщенныйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem предметToolStripMenuItem;
        private System.Windows.Forms.ListView table;
        private System.Windows.Forms.DataGridView viewm;
        private System.Windows.Forms.ToolStripMenuItem подробнаяИнформацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рекомендацииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem информацияПоПрофессиямToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem подробнаяИнформацияToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem рейтингToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem информацияОбУниверситетахToolStripMenuItem;
    }
}