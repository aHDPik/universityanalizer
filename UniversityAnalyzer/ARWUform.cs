﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Форма для редактирования значений критериев для рейтинга ARWU.
    /// </summary>
    public partial class ARWUform : Form
    {
        /// <summary>
        /// Конструктор. Создает саму форму видимые элементы на форме.
        /// </summary>
        public ARWUform()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Показывает диалоговое окно с заданными начальными критерями для их редактирования
        /// </summary>
        /// <param name="QofEducation">Качество образования</param>
        /// <param name="QofFaculty">Качество преподавательского состава</param>
        /// <param name="ResearchOutput">Научные публикации</param>
        /// <param name="PerCapita">Академическая производительность</param>
        /// <returns>Согласился ли пользователь со внесёнными изменениями</returns>
        public DialogResult ShowRatingDialog(int QofEducation, int QofFaculty, int ResearchOutput, int PerCapita)
        {
            this.QofEducation = QofEducation;
            this.QofFaculty = QofFaculty;
            this.ResearchOutput = ResearchOutput;
            this.PerCapita = PerCapita;
            return this.ShowDialog();
        }
        /// <summary>
        /// Свойство для критерия "Качество образования". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int QofEducation
        {
            get
            {
                return (int)eduVal.Value;
            }
            set
            {
                eduVal.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Качество преподавательского состава". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int QofFaculty
        {
            get
            {
                return (int)staffVal.Value;
            }
            set
            {
                staffVal.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Научные публикации". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int ResearchOutput
        {
            get
            {
                return (int)publVal.Value;
            }
            set
            {
                publVal.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Академическая производительность". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int PerCapita
        {
            get
            {
                return (int)academVal.Value;
            }
            set
            {
                academVal.Value = value;
            }
        }

        private void ARWUform_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.None)
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
