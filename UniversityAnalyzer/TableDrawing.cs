﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Вынес статический класс для рисования таблиц на форме
    /// </summary>
    public static class TableDrawing
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="g">Область для рисования</param>
        /// <param name="y">Смещение внутри таблицы</param>
        /// <param name="y_min">минимлаьное смещение</param>
        /// <param name="y_max">максимальное смещение</param>
        /// <param name="h">высота строки таблицы</param>
        /// <param name="canw">ширина холста</param>
        /// <param name="canh">высота холста</param>
        /// <param name="mas">матрица для отрисовки</param>
        public static void DrawTable(Graphics g,int y, int y_min, int y_max, int h, int canw, int canh, string[][] mas)
        {
            //y_min = this.canvas.Location.Y;  
            //создается очень много объектов абсолютно одинаковых
            y_min = 0;
            int y_width = canh;
            int x0 = 0;
            int y0 = -((y - y_min) % h);
            //что такое 4
            int width = canw - 4;
            Pen p1 = new Pen(Color.Black, 2f);
            SolidBrush b1 = new SolidBrush(Color.Gray);
            SolidBrush b2 = new SolidBrush(Color.Black);
            Font drawFont = new Font("Microsoft Sans Serif", 16);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.FillRectangle(new SolidBrush(Color.White), 0,0,canw,canh);
            //g.DrawRectangle(p1, this.DisplayRectangle);
            y_max = h * mas.Length;
            //что такое 1
            for (int i = 0; (i < y_width / h + 1) && i < mas.Length; i++)
            {
                //что такое 2
                x0 = 2;
                for (int j = 0; j < mas[(y - y_min) / h + i].Length; j++)
                {
                    //прокомментировать каждую строчку
                    Rectangle rec = new Rectangle(x0, y0, width / mas[(y - y_min) / h + i].Length, h);
                    //g.FillRectangle(b2, x0, y0, (float)(Width / mas[i].Length), hiight);
                    g.DrawRectangle(p1, rec);
                    g.DrawString(mas[(y - y_min) / h + i][j], drawFont, b2, rec, stringFormat);
                    x0 = x0 + (width / mas[(y - y_min) / h + i].Length);
                }
                y0 = y0 + h;
            }
        }
    }
}
