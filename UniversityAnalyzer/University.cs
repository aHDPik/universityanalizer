﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс Университет
    /// </summary>
    public class University
    {
        /// <summary>
        /// Структруа рекоменлация для удобной передачи в виде результата метода
        /// </summary>
        public struct Recommend
        {
            /// <summary>
            /// старый общий рейтинг предметов и новый общий рейтинг предметов
            /// </summary>
            public double totalRating, newRating;
            /// <summary>
            /// имя университета
            /// </summary>
            public String name;
            /// <summary>
            /// список новых предметов
            /// </summary>
            public List<String> newSubs;
            /// <summary>
            /// список замененных предметов
            /// </summary>
            public List<String> oldSubs;
        }
        /// <summary>
        /// профессия по умолчанию
        /// </summary>
        public const int DEFAULT_PROF = 1;
        /// <summary>
        /// Статическое поле профессий в виде таблицы где ид профессии - ключ, а название - значение
        /// </summary>
        public static Hashtable professions;
        /// <summary>
        /// Имя Униреситета
        /// </summary>
        private String name;
        /// <summary>
        /// Адрес Университета
        /// </summary>
        private String address;
        /// <summary>
        /// Рейтинг ARWU Университета
        /// </summary>
        private ARWU arwu;
        /// <summary>
        /// Рейтинг THE университета
        /// </summary>
        private THE the;
        /// <summary>
        /// Рейтинг USNews университета
        /// </summary>
        private USNews usnews;
        /// <summary>
        /// Учебный план университета
        /// </summary>
        public StudPlan studPlan;
        /// <summary>
        /// Идентификационный номер университета в таблице
        /// </summary>
        private int id;
        /// <summary>
        /// Идентификационный номер специальности в таблице
        /// </summary>
        private int profId;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя университета</param>
        /// <param name="address">Адрес Университета</param>
        /// <param name="id">Идентификационный номер университета</param>
        /// <param name="pid">Идентификационный номер специальности</param>
        public University(String name, String address, int id, int pid)
        {
            //инициализация переменных
            this.name = name;
            this.address = address;
            studPlan = new StudPlan();
            this.id = id;
            profId = pid;
            //studPlan = null;
            arwu = null;
            the = null;
            usnews = null;
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя университета</param>
        /// <param name="address">Адрес Университета</param>
        /// <param name="id">Идентификационный номер университета</param>
        public University(String name, String address, int id)
        {
            //Инициализация переменных
            this.name = name;
            this.address = address;
            studPlan = new StudPlan();
            this.id = id;
            profId = DEFAULT_PROF;
            //studPlan = null;
            arwu = null;
            the = null;
            usnews = null;

        }


        /// <summary>
        /// Добавление рейтинга ARWU 
        /// </summary>
        /// <param name="e">Уровень образования</param>
        /// <param name="f">Уровень Факультета</param>
        /// <param name="r">уровень исследований</param>
        /// <param name="p">Уровень производительности</param>
        public void AddARWU(int e, int f, int r, int p)
        {
            arwu = new ARWU(e, f, r, p);
        }

        /// <summary>
        /// Добавление рейтинга THE
        /// </summary>
        /// <param name="i">Инновации</param>
        /// <param name="l">Преподавание</param>
        /// <param name="r">Исследования</param>
        /// <param name="c">Цитирование</param>
        /// <param name="s">Сотрудники и Студенты</param>
        public void AddTHE(int i, int l, int r, int c, int s)
        {
            the = new THE(i, l, r, c, s);
        }
        /// <summary>
        /// Добавление рейтинга USNews
        /// </summary>
        /// <param name="a">Академическая репутация</param>
        /// <param name="j">Репутация у работодателей</param>
        /// <param name="l">Соотношение преподавателей и студентов</param>
        /// <param name="fl">Доля иностранных преподавателей</param>
        /// <param name="s">Доля иностранных студентов</param>
        /// <param name="c">Цитируемость в расчете на одного преподавателя</param>
        public void AddUSNews(int a, int j, int l, int fl, int s, int c)
        {
            usnews = new USNews(a, j, l, fl, s, c);
        }

        /// <summary>
        /// Добавление учебного плана
        /// </summary>
        /// <param name="plan">Идентификационный номер учебного плана в таблице</param>
        public void AddPlan(StudPlan plan)
        {
            this.studPlan = plan;
        }

        /// <summary>
        /// Добавление учебного плана
        /// </summary>
        /// <param name="pid">Идентификационный номер учебного плана в таблице</param>
        public void AddPlan(int pid)
        {
            this.studPlan = new StudPlan(pid);
        }

        /// <summary>
        /// Свойство для получения идентификационного номера университета из таблицы
        /// запись запрещена
        /// </summary>
        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// Свойство для получения учебного плана университета
        /// </summary>
        public StudPlan Plan
        {
            get { return studPlan; }
        }

        /// <summary>
        /// Информационная строка об университете, специальности, и учебном плане
        /// </summary>
        public String Info
        {
            get { return "University: " + name + ", Address: " + address + ", Profession: " + professions[profId.ToString()] + ", Plan: " + studPlan.Info; }
        }

        /// <summary>
        /// Строка информации об университете, что хранится в файле
        /// </summary>
        public String ToFile
        {
            get { return id.ToString() + ";" + name + ";" + address + ";" + profId + System.Environment.NewLine; }
        }


        /// <summary>
        /// Строка информации о ARWU рейтинге университета, что хранится в файле
        /// </summary>
        public String ARWUToFile
        {
            get { return (arwu != null) ? id.ToString() + ";" + arwu.ToFile : ""; }
        }
        /// <summary>
        /// Строка информации о THE рейтинге университета, что хранится в файле
        /// </summary>
        public String THEToFile
        {
            get { return (the != null) ? id.ToString() + ";" + the.ToFile : ""; }
        }
        /// <summary>
        /// Строка информации о USNews рейтинге университета, что хранится в файле
        /// </summary>
        public String USNewsToFile
        {
            get { return (usnews != null) ? id.ToString() + ";" + usnews.ToFile : ""; }
        }
        /// <summary>
        /// Строка информации об учебном плане университета, что хранится в файле
        /// </summary>
        public String PlanToFile
        {
            get
            {
                //мой код не рассчитывал на универы без плана
                return (studPlan != null) ? studPlan.ToFile(id) : "";
            }
        }





        // Емеля Добавляет код :

        /// <summary>
        /// Возвращает имя
        /// </summary>
        public string Name
        {
            get { return name; }
        }
        /// <summary>
        /// Возвращает/добавляет адрес университета
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        /// <summary>
        /// Возвращает ARWU
        /// </summary>
        public ARWU ARWU
        {
            get { return (arwu!=null)?arwu:new ARWU(0,0,0,0); }
        }
        /// <summary>
        /// Возвращает THE
        /// </summary>
        public THE THE
        {
            get { return (the!=null)?the:new THE(0,0,0,0,0); }
        }
        /// <summary>
        /// Возвращает USNews
        /// </summary>
        public USNews USNews
        {
            get { return (usnews!=null)?usnews:new USNews(0,0,0,0,0,0); }
        }
        /// <summary>
        /// Возвращает/добавляет id проффесии
        /// </summary>
        public int ProfId
        {
            get { return profId; }
            set { profId = value; }
        }

        ///<summary>
        /// Редактирование рейтинга ARWU
        /// </summary>
        /// <param name="e">Уровень образования</param>
        /// <param name="f">Уровень Факультета</param>
        /// <param name="r">уровень исследований</param>
        /// <param name="p">Уровень производительности</param>
        public void EditARWU(int e, int f, int r, int p)
        {
            this.arwu = new ARWU(e, f, r, p);
        }
        ///<summary>
        /// Редактирование рейтинга THE
        /// </summary>
        /// <param name="i">Инновации</param>
        /// <param name="l">Преподавание</param>
        /// <param name="r">Исследования</param>
        /// <param name="c">Цитирование</param>
        /// <param name="s">Сотрудники и Студенты</param>
        public void EditTHE(int i, int l, int r, int c, int s)
        {

            this.the = new THE(i, l, r, c, s);
        }
        ///<summary>
        /// Редактирование рейтинга USNews
        /// </summary>
        /// <param name="a">Академическая репутация</param>
        /// <param name="j">Репутация у работодателей</param>
        /// <param name="l">Соотношение преподавателей и студентов</param>
        /// <param name="fl">Доля иностранных преподавателей</param>
        /// <param name="s">Доля иностранных студентов</param>
        /// <param name="c">Цитируемость в расчете на одного преподавателя</param>
        public void EditUSNews(int a, int j, int l, int fl, int s, int c)
        {
            this.usnews = new USNews(a, j, l, fl, s, c);
        }

        /// <summary>
        /// Возвращает СтудПлан
        /// </summary>
        /// <returns>Массив массивов строк, где каждый элемент массива является ячейкой таблицы</returns>
        public string[][] GetStudPlanByUniversity()
        {
            int i = 1;
            string[][] res = new string[this.studPlan.SubjectCount + 1/* не было +1*/][];
            res[0] = new string[] { "ID", "Предмет", "№ семестра", "Вид отчетности", "Обязательный" };
            foreach (SubjectDetails sub in this.studPlan.Subjects)
            {
                res[i] = new string[5];
                res[i][0] = sub.Subj.ID.ToString();
                res[i][1] = sub.Subj.Name;
                res[i][2] = sub.Semester.ToString();
                res[i][3] = sub.Eval;
                res[i][4] = sub.Mandatory;
                i++;
            }
            return res;
        }
        /// <summary>
        /// Добавляем поле профессии к нашему универуx
        /// </summary>
        /// <param name="value">Название професии</param>
        public void setProfession(string value)
        {
            this.profId = DataBase.keyFromValue(value, University.professions);
        }
        /// <summary>
        /// общий рейтнг Университета
        /// </summary>
        /// <returns>Действительное число от 0 до 100</returns>
        public double Rate()
        {
            double arwur = (arwu != null) ? arwu.Rate() : 0;
            double ther = (the != null) ? the.Rate() : 0;
            double usnewsr = (usnews != null) ? usnews.Rate() : 0;
            return (arwur + usnewsr + ther) / 3.0;
        }
        /// <summary>
        /// Получение подробной информации об университете
        /// </summary>
        /// <returns>таблица строк с информацией</returns>
        public string[][] GetUniversityInfo()
        {
            List<string[]> InfoList = new List<string[]>();
            string[] temp = new string[1];
            temp[0] = name;
            InfoList.Add(temp);
            temp = new string[2];
            temp[0] = "Рейтинг ARWU";
            temp[1] = ARWU.Rate().ToString("0.00");
            InfoList.Add(temp);
            //ДОДЕЛАЙ
            temp = new string[2];
            //             InfoList[2][0] = "Рейтинг THE";
            //             InfoList[2][1] = Convert.ToString(THE.Rate());
            temp[0] = "Рейтинг THE";
            temp[1] = THE.Rate().ToString("0.00");
            InfoList.Add(temp);
            //             InfoList[3][0] = "Рейтинг USNews";
            //             InfoList[3][1] = Convert.ToString(USNews.Rate());
            temp = new string[2];
            temp[0] = "Рейтинг USNews";
            temp[1] = USNews.Rate().ToString("0.00");
            InfoList.Add(temp);
            //
            int maxsem = 0;
            foreach (SubjectDetails sub in this.studPlan.Subjects)
            {
                maxsem = sub.Semester > maxsem ? sub.Semester : maxsem;
            }
            for (int i = 1; i <= maxsem; i++)
            {
                bool edited = false;
                temp = new string[1];
                temp[0] = "Семестр №" + i.ToString();
                InfoList.Add(temp);
                temp = new string[4];
                temp[0] = "Название предмета";
                temp[1] = "Количество часов";
                temp[2] = "Тип занятий";
                temp[3] = "Вид отчетности";
                InfoList.Add(temp);
                foreach (SubjectDetails sub in this.studPlan.Subjects)
                {
                    // InfoList[4][0] = sub.Semester.ToString();
                    if (sub.Semester == i)
                    {
                        edited = true;
                        temp = new string[4];

                        temp[0] = sub.Subj.Name;
                        temp[1] = sub.Subj.Hours.ToString();

                        temp[2] = Subject.lessons[sub.Subj.LessonID.ToString()].ToString();

                        temp[3] = sub.Eval;
                        InfoList.Add(temp);
                    }
                }
                if (!edited)
                {
                    InfoList.RemoveRange(InfoList.Count - 2, 2);
                }
            }
            return InfoList.ToArray();
        }
        /// <summary>
        /// Удаление предмета из студ плана
        /// </summary>
        /// <param name="sub">Предмет для удаления</param>
        public void RemoveSubjectDetail(SubjectDetails sub)
        {
            Plan.Subjects.Remove(sub);
        }
        
        /// <summary>
        /// Рекомендация для выбранного университете по изменению плана
        /// </summary>
        /// <returns>Специальную структуру для преобразование в дальнейшем в специальный текст</returns>
        public Recommend Recommendation()
        {
            Recommend rec = new Recommend();
            rec.name = this.name;
            List<Subject> univSubs = new List<Subject>();
            rec.oldSubs = new List<String>();
            rec.newSubs = new List<String>();
            double newrate = 0;
            double oldrate = 0;
            foreach (SubjectDetails subj in Plan.Subjects)
            {
                if (!univSubs.Contains(subj.Subj))
                    univSubs.Add(subj.Subj);
            }
            foreach (Subject sub in univSubs)
            {
                Subject tempsub = sub;
                foreach (Subject sb in SubjectDetails.allSubjects)
                {
                    if ((!univSubs.Contains(sb)) && (!rec.newSubs.Contains(sb.Name)) && (sb.SubRating() > tempsub.SubRating()))
                    {
                        tempsub = sb;
                    }
                }
                if (tempsub != sub)
                {
                    rec.oldSubs.Add(sub.Name);
                    rec.newSubs.Add(tempsub.Name);
                }
                oldrate += sub.SubRating();
                newrate += tempsub.SubRating();
            }
            rec.totalRating = oldrate / univSubs.Count;
            rec.newRating = newrate / univSubs.Count;
            return rec;
        }
    }
}
