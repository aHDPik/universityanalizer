﻿namespace UniversityAnalyzer
{
    partial class ARWUform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.eduLab = new System.Windows.Forms.Label();
            this.staffLab = new System.Windows.Forms.Label();
            this.pubLab = new System.Windows.Forms.Label();
            this.academLab = new System.Windows.Forms.Label();
            this.eduVal = new System.Windows.Forms.NumericUpDown();
            this.staffVal = new System.Windows.Forms.NumericUpDown();
            this.publVal = new System.Windows.Forms.NumericUpDown();
            this.academVal = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.eduVal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffVal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publVal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.academVal)).BeginInit();
            this.SuspendLayout();
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Location = new System.Drawing.Point(135, 127);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(67, 25);
            this.okBtn.TabIndex = 0;
            this.okBtn.Text = "Ок";
            this.okBtn.UseVisualStyleBackColor = true;
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(208, 128);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(71, 24);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.Text = "Отмена";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // eduLab
            // 
            this.eduLab.AutoSize = true;
            this.eduLab.Location = new System.Drawing.Point(12, 20);
            this.eduLab.Name = "eduLab";
            this.eduLab.Size = new System.Drawing.Size(126, 13);
            this.eduLab.TabIndex = 2;
            this.eduLab.Text = "Качество образования:";
            // 
            // staffLab
            // 
            this.staffLab.AutoSize = true;
            this.staffLab.Location = new System.Drawing.Point(12, 45);
            this.staffLab.Name = "staffLab";
            this.staffLab.Size = new System.Drawing.Size(210, 13);
            this.staffLab.TabIndex = 3;
            this.staffLab.Text = "Качество преподавательского состава:";
            // 
            // pubLab
            // 
            this.pubLab.AutoSize = true;
            this.pubLab.Location = new System.Drawing.Point(12, 71);
            this.pubLab.Name = "pubLab";
            this.pubLab.Size = new System.Drawing.Size(116, 13);
            this.pubLab.TabIndex = 4;
            this.pubLab.Text = "Научные публикации:";
            // 
            // academLab
            // 
            this.academLab.AutoSize = true;
            this.academLab.Location = new System.Drawing.Point(12, 94);
            this.academLab.Name = "academLab";
            this.academLab.Size = new System.Drawing.Size(199, 13);
            this.academLab.TabIndex = 5;
            this.academLab.Text = "Академическая производительность:";
            // 
            // eduVal
            // 
            this.eduVal.Location = new System.Drawing.Point(277, 18);
            this.eduVal.Name = "eduVal";
            this.eduVal.Size = new System.Drawing.Size(119, 20);
            this.eduVal.TabIndex = 6;
            // 
            // staffVal
            // 
            this.staffVal.Location = new System.Drawing.Point(277, 43);
            this.staffVal.Name = "staffVal";
            this.staffVal.Size = new System.Drawing.Size(119, 20);
            this.staffVal.TabIndex = 7;
            // 
            // publVal
            // 
            this.publVal.Location = new System.Drawing.Point(277, 69);
            this.publVal.Name = "publVal";
            this.publVal.Size = new System.Drawing.Size(119, 20);
            this.publVal.TabIndex = 8;
            // 
            // academVal
            // 
            this.academVal.Location = new System.Drawing.Point(277, 95);
            this.academVal.Name = "academVal";
            this.academVal.Size = new System.Drawing.Size(119, 20);
            this.academVal.TabIndex = 9;
            // 
            // ARWUform
            // 
            this.AcceptButton = this.okBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelBtn;
            this.ClientSize = new System.Drawing.Size(409, 166);
            this.Controls.Add(this.academVal);
            this.Controls.Add(this.publVal);
            this.Controls.Add(this.staffVal);
            this.Controls.Add(this.eduVal);
            this.Controls.Add(this.academLab);
            this.Controls.Add(this.pubLab);
            this.Controls.Add(this.staffLab);
            this.Controls.Add(this.eduLab);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ARWUform";
            this.Text = "ARWU критерии";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ARWUform_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.eduVal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffVal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publVal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.academVal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label eduLab;
        private System.Windows.Forms.Label staffLab;
        private System.Windows.Forms.Label pubLab;
        private System.Windows.Forms.Label academLab;
        private System.Windows.Forms.NumericUpDown eduVal;
        private System.Windows.Forms.NumericUpDown staffVal;
        private System.Windows.Forms.NumericUpDown publVal;
        private System.Windows.Forms.NumericUpDown academVal;
    }
}