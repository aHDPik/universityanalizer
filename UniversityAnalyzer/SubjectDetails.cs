﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс детальное описание предмета
    /// </summary>
    public class SubjectDetails
    {
        /// <summary>
        /// Статическое поле типов зачетности в виде хеш-таблицы, где ид типа - ключ, название типа - значение
        /// </summary>
        public static Hashtable evals;
        /// <summary>
        /// Список всех предметов в БД
        /// </summary>
        public static List<Subject> allSubjects;
        /// <summary>
        /// Номер семестра
        /// </summary>
        private int semester;
        /// <summary>
        /// Идентификационный номер типа зачетности
        /// </summary>
        private int evalId;
        /// <summary>
        /// обяательный предмет или нет
        /// </summary>
        private bool mandatory;
        /// <summary>
        /// Предмет
        /// </summary>
        private Subject subject;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sem">Номер семестра</param>
        /// <param name="man">обяательный предмет или нет</param>
        /// <param name="eid">Идентификационный номер типа зачетности</param>
        /// <param name="sub">Предмет</param>
        public SubjectDetails(int sem, bool man, int eid, Subject sub)
        {
            //инициализируем данные
            semester = sem;
            mandatory = man;
            subject = sub;
            evalId = eid;
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sem">Номер семестра</param>
        /// <param name="man">обязательный или нет</param>
        /// <param name="evalMeth">Тип зачетности</param>
        /// <param name="subj">Название Предмета</param>
        public SubjectDetails(int sem, bool man, string evalMeth, String subj)
        {
            semester = sem;
            mandatory = man;
            int k=0;
            int i = 0;
            while ((i < allSubjects.Count) && (!allSubjects[i].Name.Equals(subj)))
                i++;
            subject = allSubjects[i];
            foreach (String key in evals.Keys)
            {
                if (evals[key].Equals(evalMeth))
                    k = Convert.ToInt32(key);
            }
            evalId = k;
        }

        /// <summary>
        /// Свойство, выдающее строку с информацией о данном предмете
        /// </summary>
        public String Info
        {
            get { return subject.Info + ", semester No" + Convert.ToString(semester) + ((mandatory) ? ", mandatory " : ", not mandatory ") + ", Evaluation: " + evals[evalId.ToString()]; }
        }

        /// <summary>
        /// Свойство, выдающее предмет как объект
        /// </summary>
        public Subject Subj
        {
            get { return this.subject; }
        }

        /// <summary>
        /// Свойство, выдающее номер семестра, в котором этот прдемет проходит
        /// </summary>
        public int Semester
        {
            get { return semester; }
        }

        /// <summary>
        /// Свойство, выдающее вид отчетности
        /// </summary>
        public string Eval
        {
            //это не массив а хеш таблица со строками ключами!!!
            get 
            { 
                //return SubjectDetails.evals[this.evalId].ToString();
                return SubjectDetails.evals[this.evalId.ToString()].ToString();
            }
        }

        /// <summary>
        /// Свойство, выдающее Обязательный ли предмет
        /// </summary>
        public string Mandatory
        {
            get { return (this.mandatory) ? "true" : "false" ; }
        }



        /// <summary>
        /// Метод, выдающий строку с информацией о предмете для записи в файл
        /// </summary>
        /// <param name="uid">Идентификационный номер предмета</param>
        /// <param name="pid">Идентификационный номер плана</param>
        /// <returns>Строку, которую следует записать в файл, чтобы сохранить инормацию о данном объекте</returns>
        public String ToFile(int uid, int pid)
        {
            return pid.ToString() + ";" + uid.ToString() + ";" + semester.ToString() + ";" + subject.ID.ToString() + ";" + evalId.ToString() +";"+ mandatory.ToString() + System.Environment.NewLine;
        }
    }
}
