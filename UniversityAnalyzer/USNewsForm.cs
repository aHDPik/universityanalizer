﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Класс - форма для редактирования значений критериев для рейтинга U.S. News.
    /// </summary>
    public partial class USNewsForm : Form
    {
        /// <summary>
        /// Конструктор. Создает саму форму видимые элементы на форме.
        /// </summary>
        public USNewsForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Показывает диалоговое окно с параметрами - начальными данными для отображения критериев
        /// </summary>
        /// <param name="AcademicReputation">Академическая репутация</param>
        /// <param name="EmployerReputation">Репутация у работодателей</param>
        /// <param name="ProportionsTeachersStudents">Соотношение преподавателей и студентов</param>
        /// <param name="ShareForeignerTeachers">Доля иностранных преподавателей</param>
        /// <param name="ShareForeignerStudents">Доля иностранных студентов</param>
        /// <param name="CititationPerPaper">Цитируемость в расчете на одного преподавателя</param>
        /// <returns>Возвращает подтвердил ли или нет пользователь изменения</returns>
        public DialogResult ShowRatingDialog(int AcademicReputation, int EmployerReputation, int ProportionsTeachersStudents, int ShareForeignerTeachers, int ShareForeignerStudents, int CititationPerPaper)
        {
            this.AcademicReputation = AcademicReputation;
            this.EmployerReputation = EmployerReputation;
            this.ProportionsTeachersStudents = ProportionsTeachersStudents;
            this.ShareForeignerTeachers = ShareForeignerTeachers;
            this.ShareForeignerStudents = ShareForeignerStudents;
            this.CititationPerPaper = CititationPerPaper;
            return this.ShowDialog();
        }
        /// <summary>
        /// Свойство для критерия "Академическая репутация". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int AcademicReputation
        {
            get
            {
                return (int)numericUpDown1.Value;
            }
            set
            {
                if ((value < 0) || (value > 100))
                {
                    numericUpDown1.Value = 0;
                }
                else numericUpDown1.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Репутация у работодателей". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int EmployerReputation
        {
            get
            {
                return (int)numericUpDown2.Value;
            }
            set
            {
                if ((value < 0) || (value > 100))
                {
                    numericUpDown2.Value = 0;
                }
                else numericUpDown2.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Соотношение преподавателей и студентов". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int ProportionsTeachersStudents
        {
            get
            {
                return (int)numericUpDown3.Value;
            }
            set
            {
                if ((value < 0) || (value > 100))
                {
                    numericUpDown3.Value = 0;
                }
                else numericUpDown3.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Доля иностранных преподавателей". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int ShareForeignerTeachers
        {
            get
            {
                return (int)numericUpDown4.Value;
            }
            set
            {
                if ((value < 0) || (value > 100))
                {
                    numericUpDown4.Value = 0;
                }
                else numericUpDown4.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Доля иностранных студентов". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int ShareForeignerStudents
        {
            get
            {
                return (int)numericUpDown5.Value;
            }
            set
            {
                if ((value < 0) || (value > 100))
                {
                    numericUpDown5.Value = 0;
                }
                else numericUpDown5.Value = value;
            }
        }
        /// <summary>
        /// Свойство для критерия "Цитируемость в расчете на одного преподавателя". Получение и редактирование. Принимает и возвращает значения [0,100]. При попытке присвоения вне диапазона [0,100] присвоится значение 0 (ноль).
        /// </summary>
        public int CititationPerPaper
        {
            get
            {
                return (int)numericUpDown6.Value;
            }
            set
            {
                if ((value < 0) || (value > 100))
                {
                    numericUpDown6.Value = 0;
                }
                else numericUpDown6.Value = value;
            }
        }
        /// <summary>
        /// Метод, выполняющийся перед закрытием формы. Задает DialogResult = Cancel.
        /// </summary>
        private void USNewsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.None)
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
