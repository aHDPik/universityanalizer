﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Абстрактный класс Rating
    /// от него наследуются классы ARWU, THE, USNews
    /// </summary>
    public abstract class Rating
    {
        /// <summary>
        /// Список критериев
        /// </summary>
        public List<int> criteria;
        /// <summary>
        /// Получение общего рейтинга по критериям
        /// </summary>
        /// <returns>общий рейтинг в виде действительного числа</returns>
        abstract public double Rate();
    }
}
