﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Рейтинг ARWU
    /// наледуется от абстрактного класса Rating
    /// </summary>
    public class ARWU:Rating
    {
        /// <summary>
        /// "прячем" список критериев
        /// </summary>
        new private List<int> criteria;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="e">Уровень образования</param>
        /// <param name="f">Уровень Факультета</param>
        /// <param name="r">уровень исследований</param>
        /// <param name="p">Уровень производительности</param>
        public ARWU(int e, int f, int r, int p)
        {
            //инициализируем данные
            criteria = new List<int>();
            criteria.Add(e);
            criteria.Add(f);
            criteria.Add(r);
            criteria.Add(p);
        }
        /// <summary>
        /// Свойства для получения Уровня образования
        /// </summary>
        public int Education
        {
            get { return criteria[0]; }
        }
        /// <summary>
        /// Свойства для получения Уровня Факультета
        /// </summary>
        public int Faculty
        {
            get { return criteria[1]; }
        }
        /// <summary>
        /// Свойства для получения Уровня исследований
        /// </summary>
        public int Research
        {
            get { return criteria[2]; }
        }
        /// <summary>
        /// Свойства для получения Уровня производительности
        /// </summary>
        public int Performance
        {
            get { return criteria[3]; }
        }
        /// <summary>
        /// метод подсчета общего рейтинга
        /// </summary>
        /// <returns>Рейтинг в виде действительного числа</returns>
        public override double Rate()
        {
            //банально считаем среднее
            int sum = 0;
            foreach (int i in criteria)
            {
                sum += i;
            }
            return sum / 4.0;
        }

        /// <summary>
        /// Свойство для создания строки для записи в файл
        /// </summary>
        public String ToFile
        {
            get
            {
                //просто записываем значение критериев через ;
                StringBuilder sb = new StringBuilder();
                foreach (int i in criteria)
                    sb.Append(i.ToString() + ";");
                sb.Append(Rate().ToString());
                sb.Append(System.Environment.NewLine);
                return sb.ToString();
            }
        }
    }
}
