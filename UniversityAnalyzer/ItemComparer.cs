﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    class ItemComparer : IComparer
    {



        private bool num;
        private SortOrder order;
        private int col;
        public ItemComparer()
        {
            num = false;
            col = 0;
            order = SortOrder.Ascending;
        }
        public ItemComparer(bool n, int column, SortOrder order)
        {
            num = n;
            col = column;
            this.order = order;
        }
        public Int32 Compare(object x, object y)
        {
            ListViewItem lx = x as ListViewItem;
            ListViewItem ly = y as ListViewItem;
            int res;
            if (num)
            {

                double dx = Convert.ToDouble(lx.SubItems[col].Text);
                double dy = Convert.ToDouble(ly.SubItems[col].Text);
                res = dx.CompareTo(dy);
            }
            else
            {
                string dx = lx.SubItems[col].Text;
                string dy = ly.SubItems[col].Text;
                res = dx.CompareTo(dy);
            }
                if (order == SortOrder.Descending)
                    res *= -1;
                return res;
            
        }

        /*public override Int32 Compare(T x, T y)
        {
            throw new Exception("The method or operation is not implemented.");
        }*/
    }
}
