﻿namespace UniversityAnalyzer
{
    partial class THEForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numerici = new System.Windows.Forms.NumericUpDown();
            this.numericl = new System.Windows.Forms.NumericUpDown();
            this.numericr = new System.Windows.Forms.NumericUpDown();
            this.numericc = new System.Windows.Forms.NumericUpDown();
            this.numerics = new System.Windows.Forms.NumericUpDown();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labels = new System.Windows.Forms.Label();
            this.labelc = new System.Windows.Forms.Label();
            this.labelrs = new System.Windows.Forms.Label();
            this.labell = new System.Windows.Forms.Label();
            this.labeli = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numerici)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numerics)).BeginInit();
            this.SuspendLayout();
            // 
            // numerici
            // 
            this.numerici.Location = new System.Drawing.Point(238, 12);
            this.numerici.Name = "numerici";
            this.numerici.Size = new System.Drawing.Size(120, 20);
            this.numerici.TabIndex = 0;
            this.numerici.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            // 
            // numericl
            // 
            this.numericl.Location = new System.Drawing.Point(238, 38);
            this.numericl.Name = "numericl";
            this.numericl.Size = new System.Drawing.Size(120, 20);
            this.numericl.TabIndex = 1;
            this.numericl.Value = new decimal(new int[] {
            34,
            0,
            0,
            0});
            // 
            // numericr
            // 
            this.numericr.Location = new System.Drawing.Point(238, 64);
            this.numericr.Name = "numericr";
            this.numericr.Size = new System.Drawing.Size(120, 20);
            this.numericr.TabIndex = 2;
            this.numericr.Value = new decimal(new int[] {
            67,
            0,
            0,
            0});
            // 
            // numericc
            // 
            this.numericc.Location = new System.Drawing.Point(238, 90);
            this.numericc.Name = "numericc";
            this.numericc.Size = new System.Drawing.Size(120, 20);
            this.numericc.TabIndex = 3;
            this.numericc.Value = new decimal(new int[] {
            51,
            0,
            0,
            0});
            // 
            // numerics
            // 
            this.numerics.Location = new System.Drawing.Point(238, 116);
            this.numerics.Name = "numerics";
            this.numerics.Size = new System.Drawing.Size(120, 20);
            this.numerics.TabIndex = 4;
            this.numerics.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(12, 142);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 15;
            this.buttonOK.Text = "ОК";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(93, 142);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 16;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 17;
            // 
            // labels
            // 
            this.labels.AutoSize = true;
            this.labels.Location = new System.Drawing.Point(15, 116);
            this.labels.Name = "labels";
            this.labels.Size = new System.Drawing.Size(125, 13);
            this.labels.TabIndex = 18;
            this.labels.Text = "Сотрудники и студенты";
            // 
            // labelc
            // 
            this.labelc.AutoSize = true;
            this.labelc.Location = new System.Drawing.Point(15, 90);
            this.labelc.Name = "labelc";
            this.labelc.Size = new System.Drawing.Size(74, 13);
            this.labelc.TabIndex = 19;
            this.labelc.Text = "Цитирование";
            // 
            // labelrs
            // 
            this.labelrs.AutoSize = true;
            this.labelrs.Location = new System.Drawing.Point(15, 64);
            this.labelrs.Name = "labelrs";
            this.labelrs.Size = new System.Drawing.Size(81, 13);
            this.labelrs.TabIndex = 20;
            this.labelrs.Text = "Исследования";
            // 
            // labell
            // 
            this.labell.AutoSize = true;
            this.labell.Location = new System.Drawing.Point(15, 38);
            this.labell.Name = "labell";
            this.labell.Size = new System.Drawing.Size(81, 13);
            this.labell.TabIndex = 21;
            this.labell.Text = "Преподавание";
            // 
            // labeli
            // 
            this.labeli.AutoSize = true;
            this.labeli.Location = new System.Drawing.Point(15, 12);
            this.labeli.Name = "labeli";
            this.labeli.Size = new System.Drawing.Size(63, 13);
            this.labeli.TabIndex = 22;
            this.labeli.Text = "Инновации";
            // 
            // THEForm
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(370, 173);
            this.Controls.Add(this.labeli);
            this.Controls.Add(this.labell);
            this.Controls.Add(this.labelrs);
            this.Controls.Add(this.labelc);
            this.Controls.Add(this.labels);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.numerics);
            this.Controls.Add(this.numericc);
            this.Controls.Add(this.numericr);
            this.Controls.Add(this.numericl);
            this.Controls.Add(this.numerici);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "THEForm";
            this.Text = "Рейтинг THE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numerici)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numerics)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labels;
        private System.Windows.Forms.Label labelc;
        private System.Windows.Forms.Label labelrs;
        private System.Windows.Forms.Label labell;
        private System.Windows.Forms.Label labeli;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.NumericUpDown numerici;
        private System.Windows.Forms.NumericUpDown numericl;
        private System.Windows.Forms.NumericUpDown numericr;
        private System.Windows.Forms.NumericUpDown numericc;
        private System.Windows.Forms.NumericUpDown numerics;
    }
}

