﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Форма для редактрирования информации о ВУЗах
    /// </summary>
    public partial class EditingInformationForm : Form
    {
        private DataBase db;
        /// <summary>
        /// Конструктор
        /// </summary>
        public EditingInformationForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Запуск диалогового окна
        /// </summary>
        /// <param name="db">База данных</param>
        /// <returns>Возвращает подтвердил ли или нет пользователь изменения</returns>
        public DialogResult ShowEditDialog(ref DataBase db)
        {
            String dp = db.folderPath;
            db.SaveToFolder(db.folderPath + @"temp_editing\");
            this.db = db;
            ConstructInformationTable();
            DialogResult dr = this.ShowDialog();
            if (dr != System.Windows.Forms.DialogResult.OK)
            {
                db = new DataBase(dp + @"temp_editing\");
                db.folderPath = dp;
            }
            Directory.Delete(dp + @"temp_editing\", true);
            return dr;
        }

        /// <summary>
        /// Метод составления таблицы с университетами и их информацией
        /// </summary>
        private void ConstructInformationTable()
        {
            view.Columns.Clear();
            String[][] uinfo = db.GetFullInfo();
            String[] allProfessions = db.GetAllProfessions();

            for (int i = 0; i < uinfo[0].Length; i++)
            {
                DataGridViewColumn d = new DataGridViewColumn();
                d.HeaderText = uinfo[0][i];
                view.Columns.Add(d);
            }
            view.Columns[0].Visible = false;
            for (int i = 1; i < uinfo.Length; i++)
            {
                DataGridViewRow d = new DataGridViewRow();
                d.Tag = db.Universities[i-1];
                for (int j = 0; j < uinfo[i].Length; j++)
                {
                    switch (j)
                    {
                        case 0://ID
                        case 1://Name
                        case 6://обобщенный
                            DataGridViewTextBoxCell c = new DataGridViewTextBoxCell();
                            c.Value = uinfo[i][j];
                            d.Cells.Add(c);
                            c.ReadOnly = true;
                            break;
                        case 2://adress
                            DataGridViewTextBoxCell cn = new DataGridViewTextBoxCell();
                            cn.Value = new TextBox().Text = uinfo[i][j];
                            d.Cells.Add(cn);
                            break;
                        case 3://arwu
                        case 4://the
                        case 5://usn
                        case 7://план обучения
                            DataGridViewButtonCell b = new DataGridViewButtonCell();
                            b.Value = "Редактировать";
                            d.Cells.Add(b);
                            break;
                        case 8:
                            DataGridViewComboBoxCell ccb = new DataGridViewComboBoxCell();
                            for (int k = 0; k < allProfessions.Length; k++) ccb.Items.Add(allProfessions[k]);
                            ccb.Value = uinfo[i][j];
                            d.Cells.Add(ccb);
                            break;
                    }
                }
                view.Rows.Add(d);
            }
            DataGridViewRow e = new DataGridViewRow();
            DataGridViewTextBoxCell hid = new DataGridViewTextBoxCell();
            e.Cells.Add(hid);
            DataGridViewButtonCell bc = new DataGridViewButtonCell();
            bc.Value = "Добавить";
            e.Cells.Add(bc);
            for (int i = 1; i < uinfo[0].Length - 1; i++)
            {
                DataGridViewTextBoxCell emptyCell = new DataGridViewTextBoxCell();
                e.Cells.Add(emptyCell);
                emptyCell.ReadOnly = true;
            }
            view.Rows.Add(e);
            //никаких численных констант
            int h = 183;
            for (int i = 1; i < view.Columns.Count - 1; i++) h += view.Columns[i].Width;
            this.Width = h;
        }

        /// <summary>
        /// Метод, выполняющийся при закрытии формы. Задает DialogReult = Cancel, если не были нажаты кнопки "Сохранить" или "Отмена"
        /// </summary>
        private void EditingInformationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        /// <summary>
        /// Метод для добавления новой специальности
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            bool newp = true;
            string[] pr = db.GetAllProfessions();
            String t = Microsoft.VisualBasic.Interaction.InputBox("Название специальности:", "Добавление специальности", "");
            if (!t.Equals(""))
            {
                for (int i = 0; i < pr.Length; i++)
                    if (pr[i].Equals(t))
                        newp = false;
                if (newp)
                {
                    db.AddProfession(t);
                    ConstructInformationTable();
                }
                else MessageBox.Show("Такая специальность уже существует");
            }
        }

        /// <summary>
        /// Метод для изменения специальности у выбранного университета
        /// </summary>
        private void view_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                int id = Convert.ToInt32((String)(view.Rows[e.RowIndex].Cells[0].Value));
                University u = db.FindUniversityByID(id);
                u.setProfession((string)((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            }
        }

        /// <summary>
        /// Метод для добавления университета или изменения критериев рейтингов указанного университета, в зависимости от нажаток кнопки в таблице
        /// </summary>
        private void view_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 3)
                {
                    ARWUform f = new ARWUform();
                    int id = Convert.ToInt32((String)(view.Rows[e.RowIndex].Cells[0].Value));
                    University u = db.FindUniversityByID(id);
                    if (u.ARWU != null)
                    {
                        if (f.ShowRatingDialog(u.ARWU.Education, u.ARWU.Faculty, u.ARWU.Research, u.ARWU.Performance) == System.Windows.Forms.DialogResult.OK)
                        {
                            u.AddARWU(f.QofEducation, f.QofFaculty, f.ResearchOutput, f.PerCapita);
                        }
                    }
                    else
                    {
                        if (f.ShowRatingDialog(0, 0, 0, 0) == System.Windows.Forms.DialogResult.OK)
                        {
                            u.AddARWU(f.QofEducation, f.QofFaculty, f.ResearchOutput, f.PerCapita);
                        }
                    }
                    //ConstructInformationTable();
                    view.Rows[e.RowIndex].Cells[6].Value = u.Rate().ToString("0.00");
                }
                else if (e.ColumnIndex == 4)
                {
                    THEForm f = new THEForm();
                    int id = Convert.ToInt32((String)(view.Rows[e.RowIndex].Cells[0].Value));
                    University u = db.FindUniversityByID(id);
                    if (u.THE != null)
                    {
                        if (f.ShowRatingDialog(u.THE.Innovation, u.THE.Lecturing, u.THE.Research, u.THE.Citation, u.THE.Students) == System.Windows.Forms.DialogResult.OK)
                        {
                            u.AddTHE(f.Innovation, f.Lecturing, f.Researching, f.Citating, f.Staff);
                        }
                    }
                    else
                    {
                        if (f.ShowRatingDialog(0, 0, 0, 0, 0) == System.Windows.Forms.DialogResult.OK)
                        {
                            u.AddTHE(f.Innovation, f.Lecturing, f.Researching, f.Citating, f.Staff);
                        }
                    }
                    //ConstructInformationTable();
                    view.Rows[e.RowIndex].Cells[6].Value = u.Rate().ToString("0.00");
                }
                else if (e.ColumnIndex == 5)
                {
                    USNewsForm f = new USNewsForm();
                    int id = Convert.ToInt32((String)(view.Rows[e.RowIndex].Cells[0].Value));
                    University u = db.FindUniversityByID(id);
                    if (u.USNews != null)
                    {
                        if (f.ShowRatingDialog(u.USNews.Academics, u.USNews.Job, u.USNews.Lecturers, u.USNews.ForeignLecturers, u.USNews.Students, u.USNews.Citations) == System.Windows.Forms.DialogResult.OK)
                        {
                            u.AddUSNews(f.AcademicReputation, f.EmployerReputation, f.ProportionsTeachersStudents, f.ShareForeignerTeachers, f.ShareForeignerStudents, f.CititationPerPaper);
                        }
                    }
                    else
                    {
                        if (f.ShowRatingDialog(0, 0, 0, 0, 0, 0) == System.Windows.Forms.DialogResult.OK)
                        {
                            u.AddUSNews(f.AcademicReputation, f.EmployerReputation, f.ProportionsTeachersStudents, f.ShareForeignerTeachers, f.ShareForeignerStudents, f.CititationPerPaper);
                        }
                    }
                    //ConstructInformationTable();
                    view.Rows[e.RowIndex].Cells[6].Value = u.Rate().ToString("0.00");
                }
                else if (e.ColumnIndex == 7)
                {
                    SPform f = new SPform();
                    int id = Convert.ToInt32((String)(view.Rows[e.RowIndex].Cells[0].Value));
                    University u = db.FindUniversityByID(id);
                    f.ShowEditDialog(ref u, ref db);
                }
                else if ((e.ColumnIndex == 1) && (e.RowIndex == view.Rows.Count - 1))
                {
                    String t = Microsoft.VisualBasic.Interaction.InputBox("Название университета:", "Добавление университета", "");
                    if (!t.Equals(""))
                    {
                        db.AddUniversity(t, "");
                        ConstructInformationTable();
                    }
                }
            }
        }

        /// <summary>
        /// Метод для изменения адреса университета
        /// </summary>
        private void view_CellEndEdit_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex==2)
            {
                db.Universities[e.RowIndex].Address = view.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
        }

        /// <summary>
        /// Метод для удаления университета
        /// </summary>
        private void remUnivBtn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow rw in view.SelectedRows)
            {
                db.RemoveUniversity(rw.Tag as University);
            }
            ConstructInformationTable();
        }
    }
}
