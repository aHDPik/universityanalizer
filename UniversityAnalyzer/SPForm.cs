﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Форма редактирования Студ Плана
    /// </summary>
    public partial class SPform : Form
    {
        /// <summary>
        /// Семестр по умолчанию
        /// </summary>
        private const int DEFAULT_SEM = 1;
        /// <summary>
        /// Задаем обязательность предмета по умолчанию "обязательным"
        /// </summary>
        private const bool DEFAULT_MAN = true;
        //костыль - надо исправить
        private const String DEFAULT_EVAL = "1";
        private const int DEFAULT_SUBJECT = 0;
        private const int DEFAULT_TABLE_SMESHENIE = 183;
        /// <summary>
        /// Поле, содержащее университет, студенческий план которого редактируется
        /// </summary>
        University univ;
        /// <summary>
        /// Поле, содержащее базу данный, в которую будут записаны исправления в случае, если пользователь нажмет кнопку "Сохранить"
        /// </summary>
        DataBase db;
        /// <summary>
        /// Конструктор
        /// </summary>
        public SPform()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Запуск диалогового окна
        /// </summary>
        /// <param name="u">Редактируемый Университет</param>
        /// <param name="db">База Данных(для сохранения)</param>
        /// <returns>Возвращает подтвердил ли или нет пользователь изменения</returns>
        public DialogResult ShowEditDialog(ref University u, ref DataBase db)
        {
            this.univ = u;
            this.db = db;
            ConstructTable();
            this.Text = "Редактирование студенческого плана " + u.Name;
            return this.ShowDialog();
        }

        /// <summary>
        /// Метод, генерирующий студенческий план университета в виде таблицы и представляющий ее на экране
        /// </summary>
        private void ConstructTable()
        {
            string[][] sp = univ.GetStudPlanByUniversity();
            string[] allSubject = db.GetAllSubjectNames();
            string[] allOtch = db.GetAllEval();
            //string[] allSubject = new string[4] {"Programming", "111", "222", "qqqq"};
            //string[] allOtch = new string[2] {"Exam", "Sem"};
            view.Columns.Clear();
            for (int i = 0; i < sp[0].Length; i++)
            {
                DataGridViewColumn d = new DataGridViewColumn();
                d.HeaderText = sp[0][i];
                view.Columns.Add(d);
            }
            view.Columns[0].Visible = false;
            for (int i = 1; i < sp.Length; i++)
            {
                DataGridViewRow d = new DataGridViewRow();
                d.Tag = univ.Plan.Subjects[i - 1];
                for (int j = 0; j < sp[i].Length; j++)
                {
                    switch (j)
                    {
                        case 0://ID
                            DataGridViewTextBoxCell a = new DataGridViewTextBoxCell();
                            a.Value = sp[i][j];
                            d.Cells.Add(a);
                            a.ReadOnly = true;
                            break;
                        case 1://Name
                            DataGridViewComboBoxCell dc = new DataGridViewComboBoxCell();
                            for (int k = 0; k < allSubject.Length; k++) dc.Items.Add(allSubject[k]);
                            dc.Value = sp[i][j];
                            d.Cells.Add(dc);
                            break;
                        case 2:
                            DataGridViewComboBoxCell ds = new DataGridViewComboBoxCell();
                            for (int k = 1; k <= 10; k++) ds.Items.Add(k.ToString());
                            ds.Value = sp[i][j];
                            d.Cells.Add(ds);
                            break;
                        case 3:
                            DataGridViewComboBoxCell dv = new DataGridViewComboBoxCell();
                            for (int k = 0; k < allOtch.Length; k++) dv.Items.Add(allOtch[k]);
                            dv.Value = sp[i][j];
                            d.Cells.Add(dv);
                            break;
                        case 4:
                            DataGridViewCheckBoxCell dcb = new DataGridViewCheckBoxCell();
                            dcb.Value = (sp[i][j] == "true") ? true : false;
                            d.Cells.Add(dcb);
                            break;
                    }
                }
                view.Rows.Add(d);
            }
            DataGridViewRow e = new DataGridViewRow();
            DataGridViewTextBoxCell hid = new DataGridViewTextBoxCell();
            e.Cells.Add(hid);
            DataGridViewButtonCell bc = new DataGridViewButtonCell();
            bc.Value = "Добавить";
            e.Cells.Add(bc);
            for (int i = 1; i <sp[0].Length - 1; i++)
            {
                DataGridViewTextBoxCell emptyCell = new DataGridViewTextBoxCell();
                e.Cells.Add(emptyCell);
                emptyCell.ReadOnly = true;
            }
            view.Rows.Add(e);
            int h = DEFAULT_TABLE_SMESHENIE;
            for (int i = 1; i < view.Columns.Count - 1; i++) h += view.Columns[i].Width;
            this.Width = h;
        }

        /// <summary>
        /// Метод, выполняющийся при нажатии кнопки "Сохранить"
        /// </summary>
        private void button4_Click(object sender, EventArgs e)
        {
            //Сохранение = Добавлению измененного студплана в университет
            StudPlan plan = new StudPlan(univ.Plan.ID);
            foreach(DataGridViewRow a in view.Rows)
            {
                if (a.Index < view.RowCount - 1)
                {
                    int sem = Convert.ToInt32(Convert.ToString(a.Cells[2].Value));
                    bool man = Convert.ToBoolean(Convert.ToString(a.Cells[4].Value));
                    String eval = Convert.ToString(a.Cells[3].Value); //Я не знаю где взять id вида отчетности
                    String sub = a.Cells[1].Value.ToString();
                    plan.AddSubject(sem, man, eval, sub);
                }
            }
            univ.AddPlan(plan);
        }

        /// <summary>
        /// Метод, выполняющийся при закрытии формы и задающий "DialogResult = Cancel", если пользователь не нажимал "Сохранить" или "Отмена", а просто закрыл форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SPform_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
                this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Метод для добавления нового вида отчетности
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            String v = Microsoft.VisualBasic.Interaction.InputBox("Введите название нового вида отчетности:", "Добавление вида отчетности", "");
            if (!v.Equals(""))
            {
                db.AddEvalMethod(v);
                ConstructTable();
            }
            else MessageBox.Show("Такой вид отчетности уже существует");
        }

        /// <summary>
        /// Метод для добавления нового предмета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            AddLessonform f = new AddLessonform();
            if(f.ShowAddDialog(ref db) == System.Windows.Forms.DialogResult.OK) ConstructTable();
        }

        /// <summary>
        /// Метод, обрабатывающий нажатие по таблице и выполняющий заранее обределеные действия, в зависимости от типа ячейки, которая была нажата
        /// </summary>
        private void view_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 1) && (e.RowIndex == view.Rows.Count - 1))
            {
                //String t = Microsoft.VisualBasic.Interaction.InputBox("Название университета:", "Добавление университета", "");
                if ((SubjectDetails.evals.Count>0)&&(SubjectDetails.allSubjects.Count>0))
                {
                    univ.Plan.AddSubject(DEFAULT_SEM, DEFAULT_MAN, SubjectDetails.evals[DEFAULT_EVAL].ToString(), SubjectDetails.allSubjects[DEFAULT_SUBJECT].Name);
                    ConstructTable();
                }
                else
                {
                    MessageBox.Show("Сначала добавьте хотя бы 1 предмет");
                }
            }
        }

        /// <summary>
        /// Метод для удаления предмета из студенческого плана университета
        /// </summary>
        private void remBtn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in view.SelectedRows)
            {
                univ.RemoveSubjectDetail(dr.Tag as SubjectDetails);
            }
            ConstructTable();
        }

    }
}
