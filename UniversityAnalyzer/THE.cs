﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Рейтинг THE
    /// наледуется от абстрактного класса Rating
    /// </summary>
    public class THE:Rating
    {
        /// <summary>
        /// "прячем" список критериев
        /// </summary>
        new private List<int> criteria;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="i">Инновации</param>
        /// <param name="l">Преподавание</param>
        /// <param name="r">Исследования</param>
        /// <param name="c">Цитирование</param>
        /// <param name="s">Сотрудники и Студенты</param>
        public THE(int i, int l, int r, int c, int s)
        {
            //инициализируем
            criteria = new List<int>();
            criteria.Add(i);
            criteria.Add(l);
            criteria.Add(r);
            criteria.Add(c);
            criteria.Add(s);
        }
        /// <summary>
        /// метод подсчета общего рейтинга
        /// </summary>
        /// <returns>Рейтинг в виде действительного числа</returns>
        public override double Rate()
        {
            //каждый критерий умножается на свой вес и сумма всех весов дает суммарный рейтинг
            return 0.025 * criteria[0] + 0.3 * criteria[1] + 0.3 * criteria[2] + 0.325 * criteria[3] + 0.05 * criteria[4];
        }

        /// <summary>
        /// Свойство для получения значения Преподавание
        /// </summary>
        public int Lecturing
        {
            get { return criteria[1]; }
        }
        /// <summary>
        /// Свойство для получения значения Исследования
        /// </summary>
        public int Research
        {
            get { return criteria[2]; }
        }
        /// <summary>
        /// Свойство для получения значения Цитирование
        /// </summary>
        public int Citation
        {
            get { return criteria[3]; }
        }
        /// <summary>
        /// Свойство для получения значения Сотрудники и Студенты
        /// </summary>
        public int Students
        {
            get { return criteria[4]; }
        }
        /// <summary>
        /// Свойство для получения значения Инновации
        /// </summary>
        public int Innovation
        {
            get { return criteria[0]; }
        }

        /// <summary>
        /// Свойство для создания строки для записи в файл
        /// </summary>
        public String ToFile
        {
            get
            {
                //просто записываем значение критериев через ;
                StringBuilder sb = new StringBuilder();
                foreach (int i in criteria)
                    sb.Append(i.ToString() + ";");
                sb.Append(Rate().ToString());
                sb.Append(System.Environment.NewLine);
                return sb.ToString();
            }
        }
    }
}
