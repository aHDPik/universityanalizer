﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Форма для добавления типа занятий
    /// </summary>
    public partial class AddLessonform : Form
    {
        DataBase db;
        /// <summary>
        /// Конструктор
        /// </summary>
        public AddLessonform()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Метод для запуска диалогового окна
        /// </summary>
        /// <param name="db">База данных</param>
        /// <returns>Принял или нет пользователь изменения</returns>
        public DialogResult ShowAddDialog(ref DataBase db)
        {
            this.db = db;
            ConstructTypes();
            return this.ShowDialog();
        }

        private void ConstructTypes()
        {
            lessonSelect.Items.Clear();
            String[] lessons = db.GetAllLessonsTypes();
            foreach (String lesson in lessons) lessonSelect.Items.Add(lesson);
            if(lessons.Length > 0)
                lessonSelect.SelectedItem = lessons[0];
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String v = Microsoft.VisualBasic.Interaction.InputBox("Введите название нового вида отчетности:", "Добавление видв отчетности", "");
            if (!v.Equals(""))
            {
                //db.AddEvalMethod(v);
                db.AddLessonTypes(v);
                ConstructTypes();
            }
            else MessageBox.Show("Такой вид отчетности уже существует");
        }

        private void AddLessonform_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.None)
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.AddSubject(nameInput.Text, Convert.ToString(hoursNum.Value), lessonSelect.Items[lessonSelect.SelectedIndex].ToString());
        }
    }
}
