﻿namespace UniversityAnalyzer
{
    partial class Recomendation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.univers = new System.Windows.Forms.ListBox();
            this.button = new System.Windows.Forms.Button();
            this.recom = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.savedial = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // univers
            // 
            this.univers.FormattingEnabled = true;
            this.univers.Location = new System.Drawing.Point(11, 41);
            this.univers.Margin = new System.Windows.Forms.Padding(2);
            this.univers.Name = "univers";
            this.univers.Size = new System.Drawing.Size(153, 186);
            this.univers.TabIndex = 0;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(188, 98);
            this.button.Margin = new System.Windows.Forms.Padding(2);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(118, 55);
            this.button.TabIndex = 1;
            this.button.Text = "Получить рекомендации";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // recom
            // 
            this.recom.Location = new System.Drawing.Point(331, 41);
            this.recom.Margin = new System.Windows.Forms.Padding(2);
            this.recom.Multiline = true;
            this.recom.Name = "recom";
            this.recom.ReadOnly = true;
            this.recom.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.recom.Size = new System.Drawing.Size(268, 186);
            this.recom.TabIndex = 2;
            this.recom.TextChanged += new System.EventHandler(this.recom_TextChanged);
            // 
            // saveBtn
            // 
            this.saveBtn.Enabled = false;
            this.saveBtn.Location = new System.Drawing.Point(467, 247);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 3;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // savedial
            // 
            this.savedial.DefaultExt = "txt";
            this.savedial.Filter = "tst файлы|*.txt";
            this.savedial.Title = "Выберите имя файла";
            // 
            // Recomendation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 291);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.recom);
            this.Controls.Add(this.button);
            this.Controls.Add(this.univers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Recomendation";
            this.ShowIcon = false;
            this.Text = "Выдача рекомендаций";
            this.Load += new System.EventHandler(this.Recomendation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox univers;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.TextBox recom;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.SaveFileDialog savedial;
    }
}