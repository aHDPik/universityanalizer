﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Рейтинг THE
    /// </summary>
    public partial class THEForm : Form
    {
        /// <summary>
        /// Конструктор формы
        /// </summary>
        public THEForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Показывает дталоговое окно с заданными начальными значениями
        /// </summary>
        /// <param name="Innovation">Инновации</param>
        /// <param name="Lecturing">Преподавание</param>
        /// <param name="Researching">Исследования</param>
        /// <param name="Citating">Цитируемость</param>
        /// <param name="Staff">Преподаватели и студенты</param>
        /// <returns>DialogResult.OK или DialogResult.Cancel</returns>
        public DialogResult ShowRatingDialog(int Innovation, int Lecturing, int Researching, int Citating, int Staff)
        {
            this.Innovation = Innovation;
            this.Lecturing = Lecturing;
            this.Researching = Researching;
            this.Citating = Citating;
            this.Staff = Staff;
            return this.ShowDialog();
        }

        /// <summary>
        /// Свойство для получения значения Инновации
        /// </summary>
        public int Innovation
        {
            get { return (int)numerici.Value; }
            set { numerici.Value = value; }
        }

        /// <summary>
        /// Свойство для получения значения Преподавание
        /// </summary>
        public int Lecturing
        {
            get { return (int)numericl.Value; }
            set { numericl.Value = value; }
        }
        /// <summary>
        /// Свойство для получения значения Исследования
        /// </summary>
        public int Researching
        {
            get { return (int)numericr.Value; }
            set { numericr.Value = value; }
        }
        /// <summary>
        /// Свойство для получения значения Цитируемость
        /// </summary>
        public int Citating
        {
            get { return (int)numericc.Value; }
            set { numericc.Value = value; }
        }
        /// <summary>
        /// Свойство для получения значения Преподаватели и студенты
        /// </summary>
        public int Staff
        {
            get { return (int)numerics.Value; }
            set { numerics.Value = value; }
        }
        /// <summary>
        /// Метод, который при закрытии формы не по кнопкам ОК или Отмена задает DialogResult = Cancel
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
                this.DialogResult = DialogResult.Cancel;
        }
    }
}
