﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace UniversityAnalyzer
{
    /// <summary>
    /// Основная форма
    /// </summary>
    public partial class mForm : Form
    {
        int mode = 0;
        /// <summary>
        /// Переменная, содержащаю базу данных.
        /// </summary>
        DataBase db;
        /// <summary>
        /// Поле, содержащее информацию о том, редактировалась ли база данных или нет
        /// </summary>
        bool edited;
        /// <summary>
        /// Конструктор основной формы.
        /// </summary>
        public mForm()
        {
            InitializeComponent();
            edited = false;
        }
        /// <summary>
        /// Метод, открывающий окно для редактировании данных о ВУЗах в базе данных.
        /// </summary>
        private void правкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Создаем окно для редактирования информации
            EditingInformationForm eif = new EditingInformationForm();
            //Открывает его как диалоговое
            if (eif.ShowEditDialog(ref db) == System.Windows.Forms.DialogResult.OK)
            {
                edited = true;
            }
        }
        /// <summary>
        /// Метод, выводящий рейтинги на форму в виде таблицы. Рейтинг выводится в зависимости от объекта, вызвавшего этот рейтинг.
        /// </summary>
        private void Rating_Click(object sender, EventArgs arg)
        {
            table.Visible = true;
            table.Clear();
            String[][] uinfo = new String[1][];
            // Тут будет определение uinfo
            int tag = Convert.ToInt32(((ToolStripMenuItem)sender).Tag.ToString());
            switch (tag)
            {
                case 3:
                    mode = 0;
                    uinfo = db.ViewUniversityARWU();
                    break;
                case 4:
                    mode = 0;
                    uinfo = db.ViewUniversityTHE();
                    break;
                case 5:
                    mode = 0;
                    uinfo = db.ViewUniversityUSNews();
                    break;
                case 6:
                    mode = 0;
                    uinfo = db.ViewUniversityCommonRating();
                    break;
                case 1:
                    mode = 1;
                    uinfo = db.ViewSubjectsGeneral();
                    break;
                default:
                    uinfo = new String[0][];
                    break;
            }
            for (int i = 0; i < uinfo[0].Length; i++)
            {
                table.Columns.Add(new ColumnHeader());
                table.Columns[i].Text = uinfo[0][i];

            }
            for (int i = 1; i < uinfo.Length; i++)
            {
                ListViewItem item = new ListViewItem(uinfo[i][0]);
                for (int j = 1; j < uinfo[i].Length; j++)
                {
                    item.SubItems.Add(uinfo[i][j]);
                }
                table.Items.Add(item);
            }
            table.View = View.Details;
            for (int i = 0; i < table.Columns.Count; i++)
            {
                table.Columns[i].Width = table.Width / table.Columns.Count;
            }

        }

        /// <summary>
        /// Метод для закрытия формы через пункт меню "Выход"
        /// </summary>
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Метод, выполняющийся перед закрытием формы. Нужен для запроса желания пользователя сохранить базу данных перед выходом
        /// </summary>
        private void mForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (edited)
            {
                DialogResult d = MessageBox.Show("Сохранить базу данных перед выходом?", "Подтверждение", MessageBoxButtons.YesNoCancel);
                if (d == System.Windows.Forms.DialogResult.Yes)
                {
                    db.Save();
                }
                else if (d != System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Метод, выполняющий сохранение базы данных
        /// </summary>
        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                db.Save();
                edited = false;
            }
            catch
            {
                MessageBox.Show("Не смог сохранить базу банных, возможно файл используется другой программой");
            }
        }

        /// <summary>
        /// Метод для открытия существующей базы данных
        /// </summary>
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool cancel = false;
            if (edited)
            {
                DialogResult d = MessageBox.Show("Сохранить текущую базу данных?", "Подтверждение", MessageBoxButtons.YesNoCancel);
                if (d == System.Windows.Forms.DialogResult.Yes)
                {
                    сохранитьToolStripMenuItem.PerformClick();
                    edited = false;
                }
                else if (d != System.Windows.Forms.DialogResult.No)
                {
                    cancel = true;
                }
            }
            if (!cancel)
            {
                FolderBrowserDialog fd = new FolderBrowserDialog();
                fd.SelectedPath = System.IO.Directory.GetCurrentDirectory();
                fd.ShowNewFolderButton = false;
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        db = new DataBase(fd.SelectedPath + @"\");
                        edited = false;
                        EnableDbButtons();
                    }
                    catch
                    {
                        MessageBox.Show("Файлов базы данных не существует, либо повреждены");
                    }
                }
            }
        }

        /// <summary>
        /// Метод для активации кнопок взаимодействия с содержанием базы данных
        /// </summary>
        private void EnableDbButtons()
        {
            сохранитьToolStripMenuItem.Enabled = true;
            правкаToolStripMenuItem.Enabled = true;
            просмотрToolStripMenuItem.Enabled = true;
            рекомендацииToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// Метод для создания новой базы данных
        /// </summary>
        private void создатьНовуюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool cancel = false;
            if (edited)
            {
                DialogResult d = MessageBox.Show("Сохранить текущую базу данных?", "Подтверждение", MessageBoxButtons.YesNoCancel);
                if (d == System.Windows.Forms.DialogResult.Yes)
                {
                    сохранитьToolStripMenuItem.PerformClick();
                    edited = false;
                }
                else if (d != System.Windows.Forms.DialogResult.No)
                {
                    cancel = true;
                }
            }
            if (!cancel)
            {
                FolderBrowserDialog fd = new FolderBrowserDialog();
                fd.SelectedPath = System.IO.Directory.GetCurrentDirectory();
                fd.ShowNewFolderButton = true;
                if (MessageBox.Show("Для создания базы нужны некоторые дополнительные данные, продолжить?", "Создание новой базы данных!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    String prof = Microsoft.VisualBasic.Interaction.InputBox("Введите название професии по умолчанию", "Профессия по умолчанию", "Программирование");
                    String eval = Microsoft.VisualBasic.Interaction.InputBox("Введите название отчетности по умолчанию", "Отчетность по умолчанию", "Зачет");
                    String les = Microsoft.VisualBasic.Interaction.InputBox("Введите название занятия по умолчанию", "Профессия по умолчанию", "Лекция");
                    if (fd.ShowDialog() == DialogResult.OK)
                    {
                        db = new DataBase(prof, eval, les, fd.SelectedPath + "\\");
                        edited = false;
                        EnableDbButtons();
                    }

                }
            }
        }

        private void mForm_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Метод, выполняющийся при изменении размера формы. Изменяет размер таблицы так, чтобы она заполняла всю форму
        /// </summary>
        private void mForm_Resize(object sender, EventArgs e)
        {
            table.Width = this.ClientSize.Width - 20;
            table.Height = this.ClientSize.Height - 40;
            for (int i = 0; i < table.Columns.Count; i++)
            {
                table.Columns[i].Width = table.Width / table.Columns.Count;
            }
        }

        private void подробнаяИнформацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InfoForm ifr = new InfoForm();
            ifr.ShowDialog(db, InfoForm.UNIVRISITIES);
        }
        /// <summary>
        /// Метод, выполняющийся при нажатии на пункт меню "рекомендации". Открывает новое окно для выдачи рекомендаций.
        /// </summary>
        private void рекомендацииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Recomendation rec = new Recomendation();
            rec.db = db;
            rec.ShowDialog();
        }

        private void информацияПоПрофессиямToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InfoForm ifr = new InfoForm();
            ifr.ShowDialog(db, InfoForm.PROFESSIONS);
        }

        private void подробнаяИнформацияToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InfoForm ifr = new InfoForm();
            ifr.ShowDialog(db, InfoForm.SUBJECTS);
        }

        private void table_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            int sortColumn = 2;

            if (table.Sorting == SortOrder.Ascending)
                table.Sorting = SortOrder.Descending;
            else
                table.Sorting = SortOrder.Ascending;

            table.ListViewItemSorter = new ItemComparer((mode == 0) && (e.Column == 2) || (mode == 1) && ((e.Column == 1) || (e.Column == 3)), e.Column,
            table.Sorting);
        }
    }
}
